git pull origin master
composer install -o
php bin/console d:m:d
php bin/console d:m:m --no-interaction
php bin/console cache:clear --env=prod
chown admin:admin -R ./
chmod -R 777 var/cache/
chmod -R 777 var/logs/
chmod -R 777 var/sessions/