<?php

namespace DoctrineExtensions\Timestampable;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class TimestampableSubscriber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return ['prePersist', 'preUpdate',];
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        $traits = class_uses($object, true);

        if ($traits && isset($traits[Timestampable::class])) {
            $object
                ->setCreatedAt(new \DateTime('now'))
                ->setUpdatedAt(new \DateTime('now'));
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        $traits = class_uses($object, true);

        if ($traits && isset($traits[Timestampable::class])) {
            $object
                ->setUpdatedAt(new \DateTime('now'));
        }
    }
}