<?php

namespace V3labs\AdminBundle\EventListener;


use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use V3labs\AdminBundle\AdminSectionController;

class AdminSectionListener
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    protected $sections;

    function __construct($twig, $sections)
    {
        $this->twig = $twig;
        $this->sections = $sections;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if (HttpKernel::MASTER_REQUEST != $event->getRequestType()) {
            return;
        }

        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof AdminSectionController) {

            $sectionName = $controller[0]->getSection();

            if (isset($this->sections[$sectionName])) {
                $this->twig->addGlobal('admin_section_name', $sectionName);
                $this->twig->addGlobal('admin_section_title', $this->sections[$sectionName]['title']);
            }
        }
    }

}