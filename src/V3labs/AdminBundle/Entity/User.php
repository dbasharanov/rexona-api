<?php

namespace V3labs\AdminBundle\Entity;

use Symfony\Component\Security\Core\User\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="admin_user")
 * @ORM\Entity
 * @Assert\UniqueEntity(fields="email", message="This e-mail is already taken.")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;
	
	/**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $sections;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;
	
	/**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $readOnly = false;
	
    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        return array('ROLE_ADMIN');
    }

    /**
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string The salt
     */
    public function getSalt()
    {
        return '';
    }

    /**
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * @return void
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }


    /**
     * @see \Serializable::serialize()
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
        ));
    }

    /**
     * @see \Serializable::unserialize()
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
	
	
	/**
     * @param string $sections
     * @return User
     */
    public function setSections($sections)
    {
        $this->sections = $sections;
    
        return $this;
    }

    /**
     * @return string
     */
    public function getSections()
    {
        return $this->sections;
    }
	
	/**
     * @param boolean $readOnly
     * @return User
     */
    public function setReadOnly($readOnly)
    {
        $this->readOnly = $readOnly;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getReadOnly()
    {
        return $this->readOnly;
    }
}
