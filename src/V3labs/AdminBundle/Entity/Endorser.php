<?php

namespace V3labs\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints as Assert;

/**
 * Endorser
 *
 * @ORM\Table(name="endorser")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EndorserRepository")
 */
class Endorser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="motto", type="string", length=255,nullable=true)
     */
    private $motto;

    /**
     * @var string
     *
     * @ORM\Column(name="youtube_id", type="string", length=255,nullable=true)
     */
    private $youtubeId;

    /**
     * @var string
     *
     * @ORM\Column(name="preview_youtube_id", type="string", length=255,nullable=true)
     */
    private $previewYoutubeId;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Endorser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Endorser
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Endorser
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set motto.
     *
     * @param string $motto
     *
     * @return Endorser
     */
    public function setMotto($motto)
    {
        $this->motto = $motto;

        return $this;
    }

    /**
     * Get motto.
     *
     * @return string
     */
    public function getMotto()
    {
        return $this->motto;
    }

    /**
     * Set youtubeId.
     *
     * @param string $youtubeId
     *
     * @return Endorser
     */
    public function setYoutubeId($youtubeId)
    {
        $this->youtubeId = $youtubeId;

        return $this;
    }

    /**
     * Get youtubeId.
     *
     * @return string
     */
    public function getYoutubeId()
    {
        return $this->youtubeId;
    }

    /**
     * Set previewYoutubeId.
     *
     * @param string $previewYoutubeId
     *
     * @return Endorser
     */
    public function setPreviewYoutubeId($previewYoutubeId)
    {
        $this->previewYoutubeId = $previewYoutubeId;

        return $this;
    }

    /**
     * Get previewYoutubeId.
     *
     * @return string
     */
    public function getPreviewYoutubeId()
    {
        return $this->previewYoutubeId;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Endorser
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

}