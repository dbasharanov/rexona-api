<?php

namespace V3labs\AdminBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class V3labsAdminExtension extends Extension implements PrependExtensionInterface {

    public function prepend(ContainerBuilder $container) {
        $container->prependExtensionConfig('twig', array(
            'form_themes' => array(
                'V3labsAdminBundle:common:form_fields.html.twig'
            )
        ));

        $container->prependExtensionConfig('liip_imagine', array(
            'filter_sets' => array(
                'admin_thumb_100x100' => array(
                    'quality' => 100,
                    'filters' => array(
                        'thumbnail' => array(
                            'size' => array(100, 100),
                            'mode' => 'outbound'
                        )
                    )
                )
            )
        ));

        $container->prependExtensionConfig('knp_paginator', array(
            'template' => array(
                'sortable' => 'V3labsAdminBundle:Pagination:sortable_link.html.twig',
                'pagination' => 'V3labsAdminBundle:Pagination:twitter_bootstrap_pagination.html.twig'
            )
        ));
    }

    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        /* Store admin sections in admin */
        $container->setParameter('v3labs_admin.sections', $config['sections']);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.xml');
    }

}
