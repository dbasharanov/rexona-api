$(function() {

    var jcropApi, cropCallback = function() {};

    $('body')
        .on('click', '.form-image .browse', function(e) {

            e.preventDefault();
            var container = $(this).parents('.form-image:first');

            V3labsFileManager.open({
                "selectCallback": function(items) {
                    var item = items[0];

                    container
                        .find('.preview')
                            .find('img').remove().end()
                            .append($('<img />').attr('src', item))
                        .end()
                        .find('input.image-url')
                            .val(item)
                        .end()
                        .find('input.image-crop')
                            .val("")
                        .end()
                }
            });
        })
        .on('mouseenter', '.form-image .preview', function() {
            if ($(this).find('img').length) {
                $(this).find('a.remove, a.crop').show();
            }
        })
        .on('mouseleave', '.form-image .preview', function() {
            $(this).find('a.remove, a.crop').hide();
        })
        .on('click', '.form-image .preview a.remove', function(e) {
            e.preventDefault();
            $(this).parents('.preview:first').find('img').remove();
            $(this).parents('.form-image:first').find('input.image-url, input.image-crop').val('');
            $(this).hide();
        })
        .on('click', '.form-image a.crop', function(e) {
            e.preventDefault();

            var src = $(this).parents('.form-image:first').find('.preview:first').find('img').attr('src'),
                cropDialog = $('#crop_dialog'),
                dialogBody = cropDialog.find('.modal-body'),
                cropInputField = $(this).find('input[type="hidden"]'),
                aspectRatio = parseFloat($(this).data('aspect-ratio'));

            if (src == "") {
                return;
            }

            if (isNaN(aspectRatio)) {
                aspectRatio = null;
            }

            dialogBody
                .find('*').remove().end()
                .append($('<img />').attr('src', src));

            dialogBody.find('img').on("load", function() {

                $('#crop_dialog').modal();

                $(this).Jcrop({ "boxWidth": 740, "boxHeight": 560, "aspectRatio": aspectRatio }, function() {
                    jcropApi = this;

                    if (cropInputField.val() != "") {
                        jcropApi.setSelect(cropInputField.val().split(","));
                    }

                    cropCallback = function() {
                        var data = jcropApi.tellSelect();

                        if (data.h > 0 && data.w > 0) {
                            cropInputField.val(Math.max(parseInt(data.x), 0) + "," + Math.max(parseInt(data.y), 0) + "," + Math.max(parseInt(data.x2), 0) + "," + Math.max(parseInt(data.y2), 0));
                        } else {
                            cropInputField.val('');
                        }

                        jcropApi.release();

                        cropDialog.modal("hide");
                    };
                })
            });
        });

    $('#crop_dialog .modal-footer .btn-primary').on('click', function(e) {
        e.preventDefault();
        cropCallback();
    });
});