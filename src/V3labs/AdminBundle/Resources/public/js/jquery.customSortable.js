(function ($) {

    $.fn.customSortable = function (param) {

        if (typeof param == "string" && param == "serialize") {

            var parents = {};
            var i = 0;
            this.find('li').each(function () {
                parents[i] = {
                    "id": $(this).data('id'),
                };

                i++;
            });

            return parents;

        }

        return this.each(function () {

            var container = $(this);

            container.find('li').disableSelection();

            container.find('li').each(function () {
                if ($(this).data('parent-id')) {
                    container.find('li[data-id="' + $(this).data('parent-id') + '"] ul:first').append($(this));
                    $(this).data('parent-id', null);
                }
            });

            container.find('ul').sortable({
                "items": "> li",
                "connectWith": container.find("ul"),
                "placeholder": "sortable-placeholder",
                "forcePlaceholderSize": true,
                "tolerance": "pointer",
                "handle": ".sortable-handle",
                "stop": function (event, ui) {

                    $.ajax({
                        "url": container.data('save-url'),
                        "type": "post",
                        "data": {
                            "data": container.customSortable('serialize')
                        },
                        "handleAs": "json",
                        "success": function (response) {

                        }
                    })

                }
            });

            container
                .on('mousedown', function () {
                    $(this).css('height', $(this).outerHeight() + 'px');
                })
                .on('mouseup', function () {
                    $(this).css('height', 'auto');
                });
        });
    };
}(jQuery));