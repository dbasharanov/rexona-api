var V3labsFileManager = {};

$(function() {

    V3labsFileManager = new function() {
        this.container = $('#file_manager');
        this.newDirContainer = $('#file_manager_new_dir');

        this.currentPath = "";
        this.selectCallback = null;
        this.selectMultiple = false;

        this.currentAjaxRequest = null;

        this.settings = {
            "vendorPath": "/vendor",
            "uploadUrl":  "/upload",
            "deleteUrl":  "/delete",
            "newDirUrl":  "/new-dir",
            "listFilesUrl": "/list-files",
			"listFilesUrlKeyword": "/list-files-keyword/kw"
        };

        this.init = function(settings) {

            if (typeof settings != "object") {
                settings = {};
            }

            $.each(settings, function(key, value) {
                V3labsFileManager.settings[key] = value;
            });
        };

        this.open = function(options) {

            var defaultOptions = {
                "selectCallback": null,
                "selectMultiple": false,
                "path": this.currentPath
            };

            if (typeof options != "object") {
                options = {};
            }

            $.each(defaultOptions, function(key, value) {
                if (typeof options[key] == "undefined") {
                    options[key] = value;
                }
            });

            this.selectMultiple = options.selectMultiple;
            this.selectCallback = options.selectCallback;

            var footer = this.container.find('.modal-footer');

            if (typeof this.selectCallback == "function") {
                footer.removeClass('hide');
            } else {
                footer.addClass('hide');
            }

            this.handleSelectButtonActivity();
            this.container.modal();
            this.listFiles(options.path);
        };

        this.listFiles = function(path) {

            if (this.currentAjaxRequest != null) {
                this.currentAjaxRequest.abort();
            }

            this.currentAjaxRequest = $.ajax({
                "url": this.settings.listFilesUrl, 
                "data": {
                    "path": path
                },
                "handleAs": 'json',
                "success": function(response) {
                    if (response.success) {
                        V3labsFileManager.container
                            .find('.breadcrumb li:gt(0)').remove().end()
                            .find('.files > div').remove().end();

                        V3labsFileManager.currentPath = response.path;

                        var dirs = response.path.split('/'),
                            tempPath = "";

                        $.each(dirs, function(key, value) {
                            if (value != "") {

                                tempPath += '/' + value;

                                $('<li />')
                                    .append($('<a />').attr('href', '#').data('path', tempPath).text(value))
                                    .appendTo(V3labsFileManager.container.find('.breadcrumb'));
                            }
                        });

                        $.each(response.items, function(key, item) {

                            var preview = $('<span />');

                            if (item.is_image) {
                                preview = $('<img />').attr('src', item.preview);
                            } else {
                                preview.addClass('glyphicon')
                                if (item.type == 'file') {
                                    preview.addClass('glyphicon-file')
                                } else {
                                    preview.addClass('glyphicon-folder-open')
                                }
                            }

                            $('<div />')
                                .data({
                                    "url": item.url,
                                    "relative-path": item.relative_path,
                                    "filename": item.filename,
                                    "type": item.type
                                })
                                .addClass("item " + item.type)
                                .append($('<input />').attr('type', 'checkbox'))
                                .append($('<span />').addClass('preview').append(preview))
                                .append($(item.type == 'dir' ? '<a href="javascript:;" />' : '<span />').addClass('label label-info').text(item.filename).attr('title', item.filename))
                                .appendTo(V3labsFileManager.container.find('.files'))
                        });

                        V3labsFileManager.handleSelectButtonActivity();
                    }
                }
            });
        };

        this.refresh = function() {
            this.listFiles(this.currentPath);
        };
		
		$('#keyword').keyup(function(){
				var self = this;
    if (self.timer)
        clearTimeout(self.timer);

    self.timer = setTimeout(function ()
    {
        self.timer = null;
        // alert('sad');
		if ($('#keyword').val()){
					url = V3labsFileManager.settings.listFilesUrlKeyword.replace("kw",$('#keyword').val());
					path = '';
				} else {
					url = V3labsFileManager.settings.listFilesUrl;
					path = '';
				}
				// alert(url);return false;
				$.ajax({
                "url": url,
                "data": {
                    "path": path
                },
                "handleAs": 'json',
                "success": function(response) {
                    if (response.success) {
                        V3labsFileManager.container
                            .find('.breadcrumb li:gt(0)').remove().end()
                            .find('.files > div').remove().end();

                        V3labsFileManager.currentPath = response.path;

                        var dirs = response.path.split('/'),
                            tempPath = "";

                        $.each(dirs, function(key, value) {
                            if (value != "") {

                                tempPath += '/' + value;

                                $('<li />')
                                    .append($('<a />').attr('href', '#').data('path', tempPath).text(value))
                                    .appendTo(V3labsFileManager.container.find('.breadcrumb'));
                            }
                        });

                        $.each(response.items, function(key, item) {

                            var preview = $('<span />');

                            if (item.is_image) {
                                preview = $('<img />').attr('src', item.preview);
                            } else {
                                preview.addClass('glyphicon')
                                if (item.type == 'file') {
                                    preview.addClass('glyphicon-file')
                                } else {
                                    preview.addClass('glyphicon-folder-open')
                                }
                            }

                            $('<div />')
                                .data({
                                    "url": item.url,
                                    "relative-path": item.relative_path,
                                    "filename": item.filename,
                                    "type": item.type
                                })
                                .addClass("item " + item.type)
                                .append($('<input />').attr('type', 'checkbox'))
                                .append($('<span />').addClass('preview').append(preview))
                                .append($(item.type == 'dir' ? '<a href="javascript:;" />' : '<span />').addClass('label label-info').text(item.filename).attr('title', item.filename))
                                .appendTo(V3labsFileManager.container.find('.files'))
                        });

                        V3labsFileManager.handleSelectButtonActivity();
                    } else {
						alert('Няма намерени файлове!');
					}
                }
            });
    }, 500);

 
				

			
		});

        this.getSelectedItems = function() {
            return this.container.find('.files > .item input[type="checkbox"]:checked').map(function() {
                return $(this).parents('.item:first').data('url');
            }).get();
        };

        this.getSelectedItemsFilenames = function() {
            return this.container.find('.files > .item input[type="checkbox"]:checked').map(function() {
                return $(this).parents('.item:first').data('filename');
            }).get();
        };

        this.handleSelectButtonActivity = function() {

            var length = this.getSelectedItems().length,
                button = this.container.find('.modal-footer .btn-primary');

            if (length == 0 || (length > 1 && this.selectMultiple == false)) {
                button.addClass('disabled');
            } else {
                button.removeClass('disabled');
            }
        };

        this.deleteFiles = function(files) {

            if (!files.length) {
                return;
            }

            $.ajax({
                "url": this.settings.deleteUrl,
                "data": {
                    "path": V3labsFileManager.currentPath,
                    "files": files
                },
                "handleAs": "json",
                "success": function(response) {
                    if (response.success) {
                        V3labsFileManager.refresh();
                    }
                }
            });
        };
		
		
    };

    V3labsFileManager.container
        .on('hidden', function() {
            V3labsFileManager.callback = null;
        })

        .on('click', '.files > .item .preview', function() {
            $(this).siblings('input[type="checkbox"]').prop('checked', !$(this).siblings('input[type="checkbox"]').prop('checked'));
            V3labsFileManager.handleSelectButtonActivity();
        })

        .on('click', '.files > .dir .label', function() {
            V3labsFileManager.listFiles($(this).parents('.dir:first').data('relative-path'));
        })
        .on('change', '.files > .item input[type="checkbox"]', function() {
            V3labsFileManager.handleSelectButtonActivity();
        })
        .on('click', '.breadcrumb a', function() {
            V3labsFileManager.listFiles($(this).data('path'));
        })
        .on('click', '.modal-body .navbar a.delete', function(e) {
            e.preventDefault();
            V3labsFileManager.deleteFiles(V3labsFileManager.getSelectedItemsFilenames());
        })
        .on('click', '.modal-footer .btn-primary', function() {
            V3labsFileManager.selectCallback(V3labsFileManager.getSelectedItems());
            V3labsFileManager.container.modal('hide');
        })
    ;

    V3labsFileManager.newDirContainer.on('show.bs.modal', function() {
        $(this)
            .find('input[type="text"]').val('').end()
            .find('.alert-danger').html('').addClass('hide').end();
    });

    V3labsFileManager.newDirContainer.on('click', 'a.submit', function() {

        $.ajax({
            "url": V3labsFileManager.settings.newDirUrl,
            "data": {
                "path": V3labsFileManager.currentPath,
                "dir":  V3labsFileManager.newDirContainer.find('input[type="text"]').val()
            },
            "handleAs": "json",
            "success": function(response) {
                if (response.success) {
                    V3labsFileManager.newDirContainer.modal('hide');
                    V3labsFileManager.refresh();
                } else {
                    V3labsFileManager.newDirContainer.find('.alert-danger').html(response.message).removeClass('hide');
                }

            }
        })
    });

   // File uploader

    var uploader = new plupload.Uploader({
        runtimes : 'gears,html5,flash,silverlight,browserplus,html4',
        browse_button : 'file_manager_upload_browse',
        container : 'file_manager_upload_container',
        max_file_size : '15mb',
        url : $('#file_manager').data('upload-url'),
        flash_swf_url : V3labsFileManager.settings.vendorPath + '/plupload/js/plupload.flash.swf',
        silverlight_xap_url : V3labsFileManager.settings.vendorPath + '/plupload/js/plupload.silverlight.xap'
    });

    uploader.init();

    uploader.bind('FilesAdded', function(up, files) {
        $.each(files, function(i, file) {
            $('#filelist').append(
                '<div id="' + file.id + '">' +
                    file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +
                '</div>');
        });


        up.settings.url = V3labsFileManager.settings.uploadUrl + V3labsFileManager.currentPath;

        uploader.start();
        up.refresh();
    });

    uploader.bind('UploadProgress', function(up, file) {
        $('#' + file.id + " b").html(file.percent + "%");

        if (uploader.total.uploaded + uploader.total.failed == uploader.files.length) {
            V3labsFileManager.refresh();
        }
    });

    uploader.bind('Error', function(up, err) {

        $('#filelist').append("<div>Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "</div>");
        up.refresh();
    });

    uploader.bind('FileUploaded', function(up, file, response) {

        var data = eval("(" + response.response + ")");

        if (data.success) {
            $('#' + file.id + " b").html("100%");
        } else {
            alert(data.message);
        }
    });
	
	
});