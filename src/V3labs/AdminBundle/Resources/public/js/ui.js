$(function() {

    $('input[type="text"].pickadate:not(.picker__input)').each(function() {

        $(this).pickadate({
            'format': $(this).data('format'),
            'formatSubmit': 'yyyy-mm-dd'
        });

    });

    $('input[type="text"].pickatime:not(.picker__input)').each(function() {

        $(this).pickatime({
            'format': $(this).data('format'),
            'formatSubmit': 'HH:i',
            'interval': 5
        });

    });

    tinymce.init({
        selector: "textarea.tinymce",
        plugins: "paste,pagebreak,image,link,code,media,email",
		toolbar2: "email",
        relative_urls: false,
        pagebreak_separator: "<!-- pagebreak -->",

        file_browser_callback: function(field_name, url, type, win) {

            V3labsFileManager.open({
                "selectCallback": function(items) {
                    win.document.getElementById(field_name).value = items[0];
                }
            });
        },
        content_style: "html { height: 100%; } body { margin: 0; padding: 8px; height: calc(100% - 16px); }"
    });

    $(document).on('click', 'a[href="#confirm_dialog"]', function(e) {
        e.preventDefault();

        $($(this).attr('href'))
            .find('.modal-footer .btn-primary').attr('href', $(this).data('href')).end()
            .modal();
    });



    /* Handle bootstrap dialog nesting */
    $('body').on('show.bs.modal', '.modal', function() {
        var modal = $(this);
        setTimeout(function() {
            $('.modal-backdrop:last').css('z-index', modal.css('z-index') - 10);
        }, 25);
    });
});