<?php

namespace V3labs\AdminBundle;


interface AdminSectionController
{
    public function getSection();
}