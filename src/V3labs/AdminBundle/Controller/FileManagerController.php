<?php

namespace V3labs\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use V3labs\AdminBundle\Entity\Media;
use V3labs\AdminBundle\FileManager\UploadHandler;

/**
 * @Route("/file-manager")
 */
class FileManagerController extends Controller
{

    protected function getWebRootDir()
    {
        return dirname($this->get('kernel')->getRootDir()) . '/web';
    }

    protected function getMediaPath()
    {
        return 'uploads';
    }

    protected function getMediaDir()
    {
        return $this->getWebRootDir() . '/' . $this->getMediaPath();
    }

    protected function getMediaUrl(Request $request)
    {
        return $request->getBasePath() . '/' . $this->getMediaPath();
    }

    protected function isInMediaDir($absolutePath)
    {
        return true;
        return !strncmp($absolutePath . '/', $this->getMediaDir() . '/', strlen($this->getMediaDir() . '/'));
    }

    /**
     * @Route("/fileuploadhandler", name="admin_fileuploadhandler")
     */
    public function fileUploadHandler(Request $request)
    {
        $output = array('uploaded' => false);
        // get the file from the request object
        $file = $request->files->get('file');

        // generate a new filename
        $fileName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();

        $date = new \DateTime();

        $uploadDir = $this->get('kernel')->getRootDir() . '/../web/uploads/' . $date->format('Y') . $date->format('m') . '/';
        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }

        if ($file->move($uploadDir, $fileName)) {
            // get entity manager
            $em = $this->getDoctrine()->getManager();
            $newDate = $date->format('Y').$date->format('m');
            // create and set this mediaEntity
            $mediaEntity = new Media();
            $mediaEntity->setName($newDate.'/'.$fileName)
                ->setCreatedAt(new \DateTime());

            // save the uploaded filename to database
            $em->persist($mediaEntity);
            $em->flush();
            $output['uploaded'] = true;
            $output['fileName'] = $fileName;
            $output['id'] = $mediaEntity->getId();
        };

        return new JsonResponse($output);
    }

    /**
     * @Route("/getfile/{id}", name="admin_getfile")
     */
    public function getFile(Media $media)
    {
        $file = $this->get('kernel')->getRootDir() . '/../web/uploads/' . $media->getName();
        return new JsonResponse([
            'path' => '/uploads/' . $media->getName(),
            'name' => $media->getName(),
            'size' => filesize($file)
        ]);
    }

    /**
     * @Route("/list-files", name="admin_file_manager_list_files")
     */
    public function listFiles(Request $request)
    {
        $response = array('success' => false, 'message' => 'An error occurred.');

        $relativePath = trim($request->get('path'), '/');
        $absolutePath = realpath($this->getMediaDir() . '/' . $relativePath);

        $cacheManager = $this->container->get('liip_imagine.cache.manager');


        if ($absolutePath !== false && is_dir($absolutePath) && $this->isInMediaDir($absolutePath)) {

            $items = array();

            foreach (new \DirectoryIterator($absolutePath) as $temp) {
                if (!$temp->isDot()) {

                    $isImage = in_array(strtolower(pathinfo($temp->getFileName(), PATHINFO_EXTENSION)), array('jpg', 'jpeg', 'png', 'bmp', 'gif'));
                    $url = $this->getMediaUrl($request) . ($relativePath !== '' ? '/' : '') . $relativePath . '/' . $temp->getFileName();

                    $items[] = array(
                        'type' => $temp->isDir() ? 'dir' : 'file',
                        'is_image' => $isImage,
                        'preview' => $isImage ? $cacheManager->getBrowserPath($url, 'admin_thumb_100x100') : null,
                        'filename' => $temp->getFileName(),
                        'relative_path' => trim($relativePath . '/' . $temp->getFileName(), '/'),
                        'url' => $url
                    );
                }
            }

            usort($items, function ($a, $b) {

                if ($a['type'] != $b['type']) {
                    return $a['type'] == 'file' ? 1 : -1;
                }

                return strtolower($a['filename']) > strtolower($b['filename']) ? 1 : -1;
            });

            unset($response['message']);
            $response['success'] = true;
            $response['path'] = $relativePath;
            $response['items'] = $items;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/new-dir", name="admin_file_manager_new_dir")
     */
    public function newDir(Request $request)
    {
        $response = array('success' => false, 'message' => 'An error occurred.');

        $relativePath = trim($request->get('path'), '/');
        $absolutePath = realpath($this->getMediaDir() . '/' . $relativePath);

        if ($absolutePath !== false && is_dir($absolutePath) && $this->isInMediaDir($absolutePath)) {

            $dir = trim($request->get('dir'));
            $newDir = $absolutePath . '/' . $dir;

            if (!preg_match("/^[a-zA-Z0-9\\-_ ]+$/", $dir)) {
                $response['message'] = 'Име на папка може да съдържа само латиснки букви, цифри, интервали, "_"  и "-".';
            } elseif (file_exists($newDir) && is_dir($newDir)) {
                $response['message'] = 'Папката вече съществува.';
            } else {

                $fs = new Filesystem();

                try {
                    $fs->mkdir($newDir);

                    unset($response['message']);
                    $response['success'] = true;
                } catch (IOException $e) {
                    $response['message'] = 'Папката не може да бъде създадена.';
                }
            }
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/upload", name="admin_file_manager_upload")
     */
    public function uploadAction(Request $request)
    {
        $relativePath = trim($request->get('path'), '/');
        $absolutePath = realpath($this->getMediaDir() . '/' . $relativePath);

        if ($absolutePath === false || !is_dir($absolutePath) || !$this->isInMediaDir($absolutePath)) {
            return new JsonResponse(array('success' => false, 'message' => 'Invalid target path.'));
        }

        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $targetDir = $absolutePath;

        $cleanupTargetDir = true;
        $maxFileAge = 5 * 3600;

        @set_time_limit(5 * 60);

        $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
        $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
        $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : '';

        // Clean the fileName for security reasons
        $fileName = preg_replace('/[^\w\._]+/', '_', $fileName);

//        if (function_exists('transliterator_transliterate')) {
//            echo $fileName = transliterator_transliterate("Any-Latin; Latin-ASCII", $fileName);
//        }

        if (!strlen($fileName)) {
            return new JsonResponse(array('success' => false, 'message' => 'No file uploaded.'));
        }

        $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

        if (!in_array($extension, array('png', 'jpg', 'jpeg', 'gif', 'bmp', 'doc', 'docx', 'xls', 'xlsx', 'pdf', 'odt', 'mp3', 'mht', 'fed', 'mp4'))) {
            return new JsonResponse(array('success' => false, 'message' => 'File extension not allowed.'));
        }

        

        $em = $this->getDoctrine()->getManager();
        // create and set this mediaEntity
        $mediaEntity = new Media();
        $mediaEntity->setName($fileName)->setCreatedAt(new \DateTime());

        // save the uploaded filename to database
        $em->persist($mediaEntity);
        $em->flush();

        // Make sure the fileName is unique but only if chunking is disabled
        if ($chunks < 2 && file_exists($targetDir . '/' . $fileName)) {
            $ext = strrpos($fileName, '.');
            $fileName_a = substr($fileName, 0, $ext);
            $fileName_b = substr($fileName, $ext);

            $count = 1;
            while (file_exists($targetDir . DIRECTORY_SEPARATOR . $fileName_a . '_' . $count . $fileName_b)) {
                $count++;
            }

            $fileName = $fileName_a . '_' . $count . $fileName_b;
        }

        $filePath = $targetDir . '/' . $fileName;

        // Create target dir
        if (!file_exists($targetDir)) {
            @mkdir($targetDir);
        }

        // Remove old temp files
        if ($cleanupTargetDir) {
            if (is_dir($targetDir) && ($dir = opendir($targetDir))) {
                while (($file = readdir($dir)) !== false) {
                    $tmpfilePath = $targetDir . DIRECTORY_SEPARATOR . $file;

                    // Remove temp file if it is older than the max age and is not the current file
                    if (preg_match('/\.part$/', $file) && (filemtime($tmpfilePath) < time() - $maxFileAge) && ($tmpfilePath != "{$filePath}.part")) {
                        @unlink($tmpfilePath);
                    }
                }
                closedir($dir);
            } else {
                return new JsonResponse(array('success' => false, 'message' => 'Failed to open temp directory.'));
            }
        }

        $contentType = null;

        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"])) {
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];
        }

        if (isset($_SERVER["CONTENT_TYPE"])) {
            $contentType = $_SERVER["CONTENT_TYPE"];
        }

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {
                // Open temp file
                $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
                if ($out) {
                    // Read binary input stream and append it to temp file
                    $in = @fopen($_FILES['file']['tmp_name'], "rb");

                    if ($in) {
                        while ($buff = fread($in, 4096))
                            fwrite($out, $buff);
                    } else {
                        return new JsonResponse(array('success' => false, 'message' => 'Failed to open input stream.'));
                    }
                    @fclose($in);
                    @fclose($out);
                    @unlink($_FILES['file']['tmp_name']);
                } else {
                    return new JsonResponse(array('success' => false, 'message' => 'Failed to open output stream.'));
                }
            } else {
                return new JsonResponse(array('success' => false, 'message' => 'Failed to move uploaded file.'));
            }
        } else {
            // Open temp file
            $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
            if ($out) {
                // Read binary input stream and append it to temp file
                $in = @fopen("php://input", "rb");

                if ($in) {
                    while ($buff = fread($in, 4096)) {
                        fwrite($out, $buff);
                    }
                } else {
                    return new JsonResponse(array('success' => false, 'message' => 'Failed to open input stream.'));
                }
                @fclose($in);
                @fclose($out);
            } else {
                return new JsonResponse(array('success' => false, 'message' => 'Failed to open output stream.'));
            }
        }

        // Check if file has been uploaded
        if (!$chunks || $chunk == $chunks - 1) {
            rename("{$filePath}.part", $filePath);

            return new JsonResponse(array('success' => true));
        }

        return new JsonResponse(array('success' => false, 'message' => 'Still uploading.'));
    }

    /**
     * @Route("/delete", name="admin_file_manager_delete")
     */
    public function deleteAction(Request $request)
    {
        $response = array('success' => false, 'message' => 'An error occurred.');

        $em = $this->get('doctrine.orm.entity_manager');

        /*$entity = $em->getRepository('V3labsAdminBundle:Media')->findByName($request->get('path'));*/
        $entity = $this->getDoctrine()
                       ->getRepository('V3labsAdminBundle:Media')
                       ->findByName($request->get('path'))[0];


        if (empty($entity)) {
            $this->get('session')->getFlashBag()->add('danger', $request->get('path'));
        } else {
            $em->remove($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'The record was deleted.');
        }

        $relativePath = trim($request->get('path'), '/');
        $absolutePath = realpath($this->getMediaDir() . '/' . $relativePath);

        if ($absolutePath !== false && is_dir($absolutePath) && $this->isInMediaDir($absolutePath)) {

            $fs = new Filesystem();

            foreach ((array)$request->get('files') as $file) {

                $path = realpath($absolutePath . '/' . $file);

                if ($path !== false && $this->isInMediaDir($path) && $path != $this->getMediaDir()) {
                    try {
                        $fs->remove($path);
                    } catch (IOException $e) {

                    }
                }
            }

            unset($response['message']);
            $response['success'] = true;
        }

        return new JsonResponse($response);
    }

    /**
     * @Route("/list-files-keyword/{keyword}", name="admin_file_manager_list_files_keyword")
     */
    public function listKeywordFiles(Request $request)
    {
        $response = array('success' => false, 'message' => 'An error occurred.');

        $relativePath = trim($request->get('path'), '/');
        $absolutePath = realpath($this->getMediaDir() . '/' . $relativePath);

        $cacheManager = $this->container->get('liip_imagine.cache.manager');
        $em = $this->get('doctrine.orm.entity_manager');


        $qb = $em->createQueryBuilder()->select('e')
            ->from('AppAppBundle:TaggedFiles', 'e')->orderBy('e.id', 'DESC')
            ->andWhere('e.name LIKE :name or e.content LIKE :name or e.author LIKE :name or e.source LIKE :name')
            ->setParameter('name', "%{$request->get('keyword')}%")
            ->getQuery()
            ->getResult();

        if (count($qb)) {

            $items = array();

            foreach ($qb as $temp) {
                $isImage = in_array(strtolower(pathinfo(basename($temp->getImageUrl()), PATHINFO_EXTENSION)), array('jpg', 'jpeg', 'png', 'bmp', 'gif'));
                $url = $temp->getImageUrl();

                $items[] = array(
                    'type' => 'file',
                    'is_image' => $isImage,
                    'preview' => $isImage ? $cacheManager->getBrowserPath($url, 'admin_thumb_100x100') : null,
                    'filename' => basename($temp->getImageUrl()),
                    'relative_path' => trim($temp->getImageUrl()),
                    'url' => $url
                );
                foreach ($temp->getPhotos() as $item) {
                    $isImage = in_array(strtolower(pathinfo(basename($item->getImageUrl()), PATHINFO_EXTENSION)), array('jpg', 'jpeg', 'png', 'bmp', 'gif'));
                    $url = $item->getImageUrl();

                    $items[] = array(
                        'type' => 'file',
                        'is_image' => $isImage,
                        'preview' => $isImage ? $cacheManager->getBrowserPath($url, 'admin_thumb_100x100') : null,
                        'filename' => basename($item->getImageUrl()),
                        'relative_path' => trim($item->getImageUrl()),
                        'url' => $url
                    );
                }
            }

            usort($items, function ($a, $b) {

                if ($a['type'] != $b['type']) {
                    return $a['type'] == 'file' ? 1 : -1;
                }

                return strtolower($a['filename']) > strtolower($b['filename']) ? 1 : -1;
            });

            unset($response['message']);
            $response['success'] = true;
            $response['path'] = $relativePath;
            $response['items'] = $items;
        }

        return new JsonResponse($response);
    }

}
