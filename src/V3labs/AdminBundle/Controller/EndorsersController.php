<?php

namespace V3labs\AdminBundle\Controller;

use V3labs\AdminBundle\Entity\Endorser;
use V3labs\AdminBundle\Form\EndorserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use V3labs\AdminBundle\AdminSectionController;
use Cocur\Slugify\Slugify;

/**
 * @Route("/endorsers")
 */
class EndorsersController extends Controller implements AdminSectionController
{
    function getSection()
    {
        return "endorsers";
    }

    /**
     * @Route("", name="admin_endorsers")
     * @Template()
     */
    function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $query = $em->createQueryBuilder()->select('e')
            ->from('V3labsAdminBundle:Endorser', 'e')->orderBy('e.name', 'ASC')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->get('page', 1), 25);

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Route("/new", name="admin_endorser_new")
     * @Template("V3labsAdminBundle:Endorsers:form.html.twig")
     */
    function newAction(Request $request)
    {
        /*$em = $this->get('doctrine.orm.entity_manager');*/

        $entity = new Endorser();
        $form = $this->createForm(EndorserType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();

            /*generateSlug($entity->getName());*/
            $slugify = new Slugify();
            $slug = $slugify->slugify($entity->getName());
            $entity->setSlug($slug);

            
            $entity->setSlug('slugs');

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_endorsers'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * @Route("/edit/{id}", name="admin_endorser_edit")
     * @Template("V3labsAdminBundle:Endorsers:form.html.twig")
     */
    function editAction(Request $request, Endorser $entity)
    {
        $form = $this->createForm(EndorserType::class, $entity, [
            'action' => $this->generateUrl('admin_endorser_edit', ['id' => $entity->getId()]),
            'edit' => true
        ]);

        $clone = clone($entity);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();

                /*generateSlug($entity->getName());*/
                $slugify = new Slugify();
                $slug = $slugify->slugify($entity->getName());
                $entity->setSlug($slug);

                
                $em->persist($entity);
                $em->flush();

                $request->getSession()
                    ->getFlashBag()
                    ->add('alert-success', 'Успешно записахте промените.');

                return $this->redirectToRoute('admin_endorsers');
            }
        }

        return [
            'form' => $form->createView(),
            'entity' => $entity
        ];
    }

    /**
     * @Route("/delete", name="admin_endorser_delete")
     */
    function deleteAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $entity = $em->getRepository('V3labsAdminBundle:Endorser')->find($request->get('id'));


        if (empty($entity)) {
            $this->get('session')->getFlashBag()->add('danger', 'Record not found.');
        } else {
            $em->remove($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'The record was deleted.');
        }

        return $this->redirect($this->generateUrl('admin_endorsers'));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(time() . uniqid());
    }

    /**
     * @return string
     */
    private function generateSlug(String $name)
    {
        $slugify = new Slugify();
        $slug = $slugify->slugify($name);
        return $slug;
    }
}