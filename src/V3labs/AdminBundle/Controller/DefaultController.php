<?php

namespace V3labs\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use V3labs\AdminBundle\Entity\User;

class DefaultController extends Controller
{

    private function getSections()
    {

        $userSections = explode('|', $this->getUser()->getSections());
        $sections = array_reverse($this->get('service_container')->getParameter('v3labs_admin.sections'), true);

        uasort($sections, function ($a, $b) {
            return $a['priority'] == $b['priority'] ? 0 : ($a['priority'] > $b['priority'] ? -1 : 1);
        });
        // проверка кои модули има
        if (count($userSections) > 1) {
            foreach ($sections as $key => $val) {
                if (!in_array($key, $userSections)) {
                    unset($sections[$key]);
                }
            }
        }
        // echo '<pre>';
        // print_R($sections);die;
        return $sections;
    }

    /**
     * @Route("/", name="_admin_home")
     * @Template()
     */
    public function indexAction()
    {
        $sections = $this->getSections();

        if (count($sections)) {
            return $this->redirect($this->generateUrl($sections[key($sections)]['route']));
        }

        // return array();
        return $this->redirect($this->generateUrl("admin_invoices"));
    }

    /**
     * @Template()
     */
    public function navigationAction($currentSection = null)
    {

        $sections = $this->getSections();

        return array(
            'sections' => $sections,
            'current_section' => $currentSection
        );
    }

    /**
     * @Route("/install")
     * @Template()
     */
    public function installAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $userCount = $em->getRepository('V3labsAdminBundle:User')->createQueryBuilder('u')
            ->select('COUNT(u)')
            ->getQuery()->getSingleScalarResult();

        if ($userCount) {
            return $this->redirect($this->generateUrl("_admin_home"));
        }

        $form = $this->createFormBuilder(new User())
            ->add('email', TextType::class, array(
                'constraints' => array(
                    new NotBlank(),
                    new Email()
                )
            ))
            ->add('password', PasswordType::class, array(
                'constraints' => array(
                    new NotBlank(),
                    new Length(array('min' => 5))
                )
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            $user = $form->getData();

            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $user->setPassword($encoder->encodePassword($user->getPassword(), $user->getSalt()));

            $user->setName('Administrator');

            $em->persist($user);
            $em->flush();

            return $this->redirect($this->generateUrl("_admin_home"));
        }

        return array(
            'form' => $form->createView()
        );
    }

}
