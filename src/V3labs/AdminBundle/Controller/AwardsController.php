<?php

namespace V3labs\AdminBundle\Controller;

use V3labs\AdminBundle\Entity\Award;
use V3labs\AdminBundle\Form\AwardType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use V3labs\AdminBundle\AdminSectionController;
use Cocur\Slugify\Slugify;

/**
 * @Route("/awards")
 */
class AwardsController extends Controller implements AdminSectionController
{
    function getSection()
    {
        return "awards";
    }

    /**
     * @Route("", name="admin_awards")
     * @Template()
     */
    function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $query = $em->createQueryBuilder()->select('e')
            ->from('V3labsAdminBundle:Award', 'e')->orderBy('e.name', 'ASC')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->get('page', 1), 25);

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Route("/new", name="admin_award_new")
     * @Template("V3labsAdminBundle:Awards:form.html.twig")
     */
    function newAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $entity = new Award();
        $form = $this->createForm(AwardType::class, $entity);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_awards'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * @Route("/edit/{id}", name="admin_award_edit")
     * @Template("V3labsAdminBundle:Awards:form.html.twig")
     */
    function editAction(Request $request, Award $entity)
    {
        $form = $this->createForm(AwardType::class, $entity, [
            'action' => $this->generateUrl('admin_award_edit', ['id' => $entity->getId()]),
            'edit' => true
        ]);

        $clone = clone($entity);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();

                $entityManager->persist($entity);
                $entityManager->flush();

                $request->getSession()
                    ->getFlashBag()
                    ->add('alert-success', 'Успешно записахте промените.');

                return $this->redirectToRoute('admin_awards');
            }
        }

        return [
            'form' => $form->createView(),
            'entity' => $entity
        ];
    }

    /**
     * @Route("/delete", name="admin_award_delete")
     */
    function deleteAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $entity = $em->getRepository('V3labsAdminBundle:Award')->find($request->get('id'));


        if (empty($entity)) {
            $this->get('session')->getFlashBag()->add('danger', 'Record not found.');
        } else {
            $em->remove($entity);
            $em->flush();
            
            $this->get('session')->getFlashBag()->add('success', 'The record was deleted.');
        }

        return $this->redirect($this->generateUrl('admin_awards'));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        return md5(time() . uniqid());
    }

}