<?php

namespace V3labs\AdminBundle\Controller;

use V3labs\AdminBundle\Entity\User;
use V3labs\AdminBundle\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use V3labs\AdminBundle\AdminSectionController;

/**
 * @Route("/users")
 */
class UserController extends Controller implements AdminSectionController
{
    function getSection()
    {
        return "users";
    }

    /**
     * @Route("/", name="admin_users")
     * @Template()
     */
    function indexAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $query = $em->createQueryBuilder()->select('e')
            ->from('V3labsAdminBundle:User', 'e')->orderBy('e.name', 'ASC')
            ->getQuery();

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate($query, $request->get('page', 1), 25);

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Route("/new", name="admin_user_new")
     * @Template("V3labsAdminBundle:User:form.html.twig")
     */
    function newAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $entity = new User();
        $form = $this->createForm(UserType::class, $entity);

        $form->handleRequest($request);

        if ($form->isValid()) {

            $encoder = $this->get('security.encoder_factory')->getEncoder($entity);
            $entity->setPassword($encoder->encodePassword($form->get('password')->getData(), $entity->getSalt()));

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'New record was created.');
            return $this->redirect($this->generateUrl('admin_users'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }

    /**
     * @Route("/edit/{id}", name="admin_user_edit")
     * @Template("V3labsAdminBundle:User:form.html.twig")
     */
    function editAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $entity = $em->getRepository('V3labsAdminBundle:User')->find($request->get('id'));

        if (empty($entity)) {
            $this->get('session')->getFlashBag()->add('danger', 'Record not found.');
            return $this->redirect($this->generateUrl('admin_users'));
        }

        $form = $this->createForm(UserType::class, $entity, array('is_edit' => true));

        $form->handleRequest($request);

        if ($form->isValid()) {

            $password = $form->get('password')->getData();

            if ($password) {
                $encoder = $this->get('security.encoder_factory')->getEncoder($entity);
                $entity->setPassword($encoder->encodePassword($password, $entity->getSalt()));
            }

            $em->persist($entity);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'The record was updated.');
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView()
        );
    }
    /**
     * @Route("/delete", name="admin_user_delete")
     */
    function deleteAction(Request $request)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        $entity = $em->getRepository('V3labsAdminBundle:User')->find($request->get('id'));

        if ($entity == $this->getUser()) {
            $this->get('session')->getFlashBag()->add('danger', 'You cannot delete yourself.');
        } else {

            if (empty($entity)) {
                $this->get('session')->getFlashBag()->add('danger', 'Record not found.');
            } else {
                $em->remove($entity);
                $em->flush();

                $this->get('session')->getFlashBag()->add('success', 'The record was deleted.');
            }
        }

        return $this->redirect($this->generateUrl('admin_users'));
    }
}