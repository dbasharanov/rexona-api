<?php

namespace V3labs\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class UserSessionController extends Controller {

    /**
     * @Route("/login", name="_admin_login")
     * @Template()
     */
    public function loginAction(Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return array(
            'last_username' => $lastUsername,
            'error' => $error
        );
    }

    /**
     * @Route("/logout", name="_admin_logout")
     * @Template()
     */
    public function logoutAction() {
        return [];
    }

    /**
     * @Route("/login-check", name="_admin_login_check")
     * @Template()
     */
    public function loginCheckAction() {
        return [];
    }

}
