<?php

namespace V3labs\AdminBundle\Twig;

class ImageCropExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('image_crop', array($this, 'imageCrop')),
        );
    }

    public function imageCrop($path, $crop = null)
    {
        if ($crop !== null) {
            if (preg_match_all("/^\d+,\d+,\d+,\d+$/", $crop)) {
                return "crop_{$crop}/" . ltrim($path, '/');
            } else {
                //throw new \InvalidArgumentException(sprintf('"%s" is not a valid crop setting', $crop));
            }
        }

        return $path;
    }

    public function getName()
    {
        return 'image_crop_extension';
    }
}