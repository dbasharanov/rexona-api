<?php

namespace V3labs\AdminBundle\Imagine\Data\Loader;

use Imagine\Image\Box;
use Imagine\Image\Point;
use Liip\ImagineBundle\Imagine\Data\Loader\FileSystemLoader;
use Symfony\Component\HttpFoundation\Request;

class FileSystemCropLoader extends FileSystemLoader
{
    /**
     * {@inheritDoc}
     */
    public function find($path)
    {
        if (preg_match("/^\/*crop_(\d+),(\d+),(\d+),(\d+)\//", $path, $matches)) {

            $path = substr($path, strlen($matches[0]));

            list($x1, $y1, $x2, $y2) = array($matches[1], $matches[2], $matches[3], $matches[4]);

            $image = parent::find($path);
            $image->crop(new Point($x1, $y1), new Box($x2 - $x1, $y2 - $y1));

            return $image;
        }

        return parent::find($path);
    }
}