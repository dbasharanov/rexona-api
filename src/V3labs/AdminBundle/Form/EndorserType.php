<?php

namespace V3labs\AdminBundle\Form;

use V3labs\AdminBundle\Entity\Endorser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EndorserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
            ->add('name', TextType::class, array('label' => 'Име', 'constraints' => new NotBlank()))
            ->add('description', TextareaType::class, [
                'label' => 'Описание',
                'attr' => ['rows' => 10,'class' => 'tinymce'],
                'required' => true
            ])
            ->add('motto', TextType::class, array('label' => 'Мотивиращо изречение '))
            ->add('image', FileType::class, array('label' => 'Image'))
            ->add('youtube_id', TextType::class, array('label' => 'YouTube Id', 'constraints' => new NotBlank()))
            ->add('preview_youtube_id', TextType::class, array('label' => 'Short Video YouTube Id', 'constraints' => new NotBlank()))
            ->add('image', HiddenType::class, array('label' => 'Снимка 1', 'required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Endorser::class,
            'edit' => false
        ));
    }

}
