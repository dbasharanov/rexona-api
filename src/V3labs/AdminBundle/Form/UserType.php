<?php

namespace V3labs\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class UserType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
            ->add('name', TextType::class, array('label' => 'Name', 'constraints' => new NotBlank()))
            ->add('email', TextType::class, array('label' => 'Email', 'constraints' => array(new NotBlank(), new Email())))
            ->add('sections', TextType::class, array('label' => 'Sections'))
            ->add('readOnly', CheckboxType::class, array('label' => 'Read Only User', 'required' => false))
        ;

        $constraints = array();

        if (!$options['is_edit']) {
            $constraints[] = new NotBlank();
        }
        $constraints[] = new Length(array('min' => 6));

        $builder->add('password', TextType::class, array(
            'label' => 'Password',
            'constraints' => $constraints,
            'required' => !$options['is_edit'],
            'mapped' => false
        ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => 'V3labs\AdminBundle\Entity\User',
            'is_edit' => false
        ])->setRequired([
            'is_edit'
        ]);
    }

    public function getName() {
        return 'v3labs_admin_user';
    }

}
