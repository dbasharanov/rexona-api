<?php

namespace V3labs\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Weemss\UserBundle\Entity\Organization;

class DateTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('date', TextType::class, array(
            'attr' => array(
                'readonly' => 'readonly',
                'data-format' => $options['date_format'],
                'class' => 'pickadate'
            )
        ));

        if (null !== $options['time_format']) {
            $builder->add('time', TextType::class, array(
                'attr' => array(
                    'readonly' => 'readonly',
                    'data-format' => $options['time_format'],
                    'class' => 'pickatime'
                )
            ));
        }

        $builder->addModelTransformer(new DateTimeTransformer($options));
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'date_format' => null,
            'time_format' => null,
            'timezone'    => date_default_timezone_get(),
            'error_bubbling' => false,
            'data_class' => null
        ));

        $resolver->setRequired(array('date_format'));
    }

    function getParent()
    {
        return 'form';
    }

    function getName()
    {
        return 'v3labs_admin_datetime';
    }

}