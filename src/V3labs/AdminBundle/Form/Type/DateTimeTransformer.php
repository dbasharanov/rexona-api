<?php

namespace V3labs\AdminBundle\Form\Type;

use Symfony\Component\Form\DataTransformerInterface;
use DateTime;
use DateTimeZone;

class DateTimeTransformer implements DataTransformerInterface
{
    protected $options;

    public function __construct($options)
    {
        $this->options = $options;
    }

    public function transform($date)
    {
        if (empty($date)) {
            return null;
        }

        $date->setTimezone(new DateTimeZone($this->options['timezone']));

        return array(
            'date' => $date->format('Y-m-d'),
            'time' => $date->format('H:i')
        );
    }

    public function reverseTransform($data)
    {

        if (empty($data['date']) && empty($data['time'])) {
            return null;
        }

        $datetime = new \DateTime("{$data['date']}" . (isset($data['time']) ? " {$data['time']}" : ""), new DateTimeZone($this->options['timezone']));

        return $datetime;
    }
}