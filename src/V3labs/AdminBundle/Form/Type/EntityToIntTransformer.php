<?php

namespace V3labs\AdminBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class EntityToIntTransformer implements DataTransformerInterface
{
    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;
    private $entityClass;

    /**
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $entity
     *
     * @throws \Symfony\Component\Form\Exception\TransformationFailedException
     *
     * @return integer
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return null;
        }

        if (!$entity instanceof $this->entityClass) {
            throw new TransformationFailedException("An instance of $this->entityClass must be provided");
        }

        return $entity->getId();
    }

    /**
     * @param mixed $id
     *
     * @throws \Symfony\Component\Form\Exception\TransformationFailedException
     *
     * @return mixed|object
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $entity = $this->em->getRepository($this->entityClass)->findOneBy(array('id' => $id));

        if (null === $entity) {
            throw new TransformationFailedException(sprintf('A %s with id "%s" does not exist.', $this->entityClass, $id));
        }

        return $entity;
    }

    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;
    }
}