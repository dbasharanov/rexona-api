<?php

namespace V3labs\AdminBundle\Form;

use V3labs\AdminBundle\Entity\Award;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AwardType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {


        $builder
            ->add('name', TextType::class, array('label' => 'Име', 'constraints' => new NotBlank()))
            ->add('number', TextType::class, array('label' => 'Брой', 'constraints' => array(new NotBlank())))
            ->add('description', TextType::class, array('label' => 'Описание', 'constraints' => new NotBlank()))
            ->add('image', HiddenType::class, array('label' => 'Снимка 1', 'required' => false));
            
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Award::class,
            'edit' => false
        ));
    }

}
