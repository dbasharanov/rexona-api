<?php

namespace AppBundle\Domain\Image;

class Filter
{
    /**
     * @var int Sets the width of the image, in pixels
     */
    private $width;

    /**
     * @var int Sets the height of the image, in pixels
     */
    private $height;

    /**
     * @var int Adds a blur effect to the image
     */
    private $blur;

    /**
     * @var int[] The size of the background generated image
     */
    private $backgroundGenerator;

    /**
     * @var [string, int] The color and opacity of the overlay
     */
    private $overlay;

    /**
     * @var string Resizes the image to fit the width and height boundaries and crops excess image data
     */
    private $fit;

    /**
     * @var int[] Crops an image by given [width, height, x, y]
     */
    private $crop;

    /**
     * Filter constructor
     */
    public function __construct()
    {
    }

    /**
     * @return int | null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return int | null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return int
     */
    public function getBlur()
    {
        return $this->blur;
    }

    /**
     * @param int $width
     * @return $this
     */
    public function setWidth($width)
    {
        if (!isset($this->width)) {
            $this->width = $width;
        }

        return $this;
    }

    /**
     * @param int $height
     * @return $this
     */
    public function setHeight($height)
    {
        if (!isset($this->height)) {
            $this->height = $height;
        }

        return $this;
    }

    /**
     * @param int $blur
     * @return $this
     */
    public function setBlur($blur)
    {
        if (!isset($this->blur)) {
            $this->blur = $blur;
        }

        return $this;
    }

    /**
     * @return \int[]
     */
    public function getBackgroundGenerator()
    {
        if (empty($this->backgroundGenerator) || count($this->backgroundGenerator) !== 2) {
            return null;
        }

        return $this->backgroundGenerator[0] . ',' . $this->backgroundGenerator[1];
    }

    /**
     * @param $width
     * @param $height
     * @return $this
     */
    public function setBackgroundGenerator($width, $height)
    {
        if (!isset($this->backgroundGenerator)) {
            $this->backgroundGenerator = [$width, $height];
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOverlay()
    {
        if (empty($this->overlay)) {
            return null;
        }

        return $this->overlay[0] . ',' . $this->overlay[1];
    }

    /**
     * @param $color
     * @param $opacity
     * @return $this
     */
    public function setOverlay($color, $opacity)
    {
        if (!isset($this->overlay)) {
            $this->overlay = [$color, $opacity];
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getFit()
    {
        return $this->fit;
    }

    /**
     * @param string $fit
     * @return $this
     */
    public function setFit($fit)
    {
        if (!isset($this->fit)) {
            $this->fit = $fit;
        }

        return $this;
    }

    /**
     * @return \int[]
     */
    public function getCrop()
    {
        if (empty($this->crop)) {
            return null;
        }

        return implode(',', $this->crop);
    }

    /**
     * @param $width
     * @param $height
     * @param $x
     * @param $y
     * @return $this
     */
    public function setCrop($width, $height, $x, $y)
    {
        if (!isset($this->crop)) {
            $this->crop = [$width, $height, $x, $y];
        }

        return $this;
    }
}
