<?php
/**
 * User: Ivo Stefanov
 * Date: 5/13/16
 */

namespace AppBundle\Domain\Image;

interface FilterFactoryInterface
{
    /**
     * Available filter names
     */
    const PLAIN_FILTER = 'plain_filter';
    const CANDIDATE_PHOTO_FILTER = 'candidate_photo_filter';
    const CANDIDATE_PHOTO_BLURRED_FILTER = 'candidate_blurred_filter';
    const VENUE_LOGO_FILTER = 'venue_logo_filter';
    const VENUE_LIST_PHOTO_FILTER = 'venue_list_photo_filter';
    const VENUE_PHOTO_FILTER = 'venue_photo_filter';
    const VENUE_PHOTO_BLURRED_FILTER = 'venue_blurred_filter';
    const VENUE_LOGO_BACKGROUND_FILTER = 'venue_logo_background_filter';
    const VENUE_LOGO_SOLID_BACKGROUND_FILTER = 'venue_logo_solid_background_filter';

    /**
     * Create an image filter based on the type of image required
     * @param $filterName
     * @return Filter
     */
    public function create($filterName);
}