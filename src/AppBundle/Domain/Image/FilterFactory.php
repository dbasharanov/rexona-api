<?php
/**
 * User: Ivo Stefanov
 * Date: 13/5/16
 */

namespace AppBundle\Domain\Image;

use InvalidArgumentException;

class FilterFactory implements FilterFactoryInterface
{
    /**
     * @var array holds callbacks to existing filters constructor callbacks
     */
    private $filters;

    /**
     * FilterFactory constructor
     */
    public function __construct()
    {
        $this->filters = [
            self::CANDIDATE_PHOTO_FILTER => function () {
                return (new Filter())->setWidth(190)->setHeight(190)->setFit('crop');
            },
            self::CANDIDATE_PHOTO_BLURRED_FILTER => function () {
                return (new Filter())->setWidth(750)->setHeight(290)->setFit('crop')->setBlur(1)->setOverlay('#000', 80);
            },
            self::VENUE_LIST_PHOTO_FILTER => function () {
                return (new Filter())->setWidth(190)->setHeight(190)->setFit('crop');
            },
            self::VENUE_PHOTO_FILTER => function () {
                return (new Filter())->setWidth(750)->setHeight(290)->setFit('crop');
            },
            self::VENUE_LOGO_FILTER => function () {
                return (new Filter())->setHeight(208);
            },
            self::VENUE_PHOTO_BLURRED_FILTER => function () {
                return (new Filter())->setWidth(750)->setHeight(290)->setFit('crop')->setBlur(1)->setOverlay('#000', 80);
            },
            self::PLAIN_FILTER => function () {
                return (new Filter());
            },
            self::VENUE_LOGO_BACKGROUND_FILTER => function () {
                return (new Filter())->setBackgroundGenerator(750, 208);
            },
            self::VENUE_LOGO_SOLID_BACKGROUND_FILTER => function () {
                return (new Filter())->setWidth(750)->setHeight(208)->setOverlay('#fff', 100);
            }
        ];
    }

    /**
     * Create an image filter based on the type of image required
     * @param $filterName
     * @return $this
     */
    public function create($filterName)
    {
        if (!isset($this->filters[$filterName])) {
            throw new InvalidArgumentException(sprintf('Filter "%s" is not on the list with available filters.', $filterName));
        }
        return $this->filters[$filterName]();
    }
}
