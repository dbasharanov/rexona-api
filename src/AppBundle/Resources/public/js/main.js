document.addEventListener("DOMContentLoaded", function(event) { 
  
  toggleMenu();
  openPopup();  
  showEmbedVideo();

  if (getCookie("rexona_cookie_consent") == "1") {
    startHomeAnimaton();
  } else {
    startHomeVideo();
  }


const players = Plyr.setup('.js-player');
    
$( "div.media-wrapper" )
  .mouseenter(function() {
    id = $(this).parent().attr('id');
    players[id].play();
    players[id].volume = 0;
    function startPlayer(){
      $( '#' + id + ' img.cover-image').hide();
      players[id].speed = 2;
      $(this).children('.plyr').show();
    };
    window.setTimeout( startPlayer, 1000 ); 
    players[id].on('ended', event => {
      players[id].play();     
    });
  })
  .mouseleave(function() {
    $( '#' + id + ' img.cover-image').show();
    id = $(this).parent().attr('id');
    $(this).children('.plyr').hide();
    players[id].restart();
    players[id].stop();
  }); 

});

function toggleMenu() {
  $('.hamburger').click(function (e) { 
    $('.hamburger').toggleClass('is-active');
    $("nav").toggle();
  });
}

function openPopup() {
  var container = $(".modal");
  $('.open-popup').click(function (e) { 
  $(".modal").removeClass('is-active');
    modalName = $(this).attr('data-open');
    console.log(modalName);
    $('#'+ modalName).wrap( "<div class='modal-cover'></div>" );
    $('#'+ modalName).addClass('is-active');
  });
  $('.icon-close').click(function (e) { 
    container.unwrap( "<div class='modal-cover'></div>" );
    container.removeClass('is-active');
  });
  
  $(document).keydown(function(ev) {
    if (ev.keyCode == 27) {
      container.unwrap( "<div class='modal-cover'></div>" );
      container.removeClass('is-active');
    }
  });
}


function startHomeVideo() {
  const player = new Plyr('#player');
  player.on('ready', event => {
    player.play();
    player.volume = 0;
  });  
  player.on('ended', event => {
    const instance = event.detail.plyr;
    updateCookie("rexona_cookie_consent", 1, 365);
    player.destroy();
    startHomeAnimaton();
  });
}

function startHomeAnimaton() {
  if(document.getElementById("bannerAreaWrap") !== null) {
    delay = 0;
    TweenMax.to(bannerAreaWrap, 0, {display: 'block'});
    TweenMax.to(peak, 0.5, {opacity:1,y:-20, delay: delay += 1});
    TweenMax.to(slogan, 0.5, {opacity:1,y:20, delay: delay += 1});
    TweenMax.to(counter, 0.5, {opacity:1, delay: delay += 1});
    TweenMax.to(path, 0.5, {opacity:1, delay: delay += 1});
    TweenMax.to(text, 0.5, {opacity:1, delay: delay += 1});
    TweenMax.to(endorser1, 0.5, {opacity:1, y:10, delay: delay += 1});
    TweenMax.to(endorser2, 0.5, {opacity:1, y:10, delay: delay += 1});
    TweenMax.to(endorser3, 0.5, {opacity:1, y:10, delay: delay += 1});
    TweenMax.to(endorser4, 0.5, {opacity:1, y:10, delay: delay += 1});
    TweenMax.to(endorser5, 0.5, {opacity:1, y:10, delay: delay += 1});
    TweenMax.to(endorser6, 0.5, {opacity:1, y:10, delay: delay += 1});
    delay += 2.5; 
  }
}

function showEmbedVideo(){
  $('#playVideo').click(function (e) { 
    $(this).parent().hide();
    const player = new Plyr('#endorserPlayer');
    player.on('ready', event => {
      player.play();
      player.speed = 1;
      player.volume = 0;
    });  
  });
}



function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
    }
  }
  return "";  
}

function updateCookie(cname,value,exdays) {
  var now = new Date();
  var time = now.getTime();

  time += exdays * 24 * 60* 60 * 1000;
  now.setTime(time);
  document.cookie = cname + '=' + value + ';' +
                    'path=/;' + 
                    'domain=.' + window.location.host + ';' +  
                    'expires=' + now.toUTCString(); 
}

