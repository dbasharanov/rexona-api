<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UploadedFile as UploadedFileEntity;

interface UploadedFileRepositoryInterface
{
    /**
     * @param UploadedFileEntity $uploadedFile
     */
    public function save(UploadedFileEntity $uploadedFile);

    /**
     * @param array $criteria
     * @param array $orderBy
     * @return UploadedFileEntity
     */
    public function findOneBy(array $criteria, array $orderBy = null);
}
