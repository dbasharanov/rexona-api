<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UploadedFile as UploadedFileEntity;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityRepository;

class UploadedFileRepository extends EntityRepository implements UploadedFileRepositoryInterface
{
//    /**
//     * @var EntityManager
//     */
//    private $em;
//
//    /**
//     * @var LoggerInterface
//     */
//    protected $logger;
//
//    public function __construct($em, $logger)
//    {
//        $this->em = $em;
//        $this->logger = $logger;
//        parent::__construct($this->em, $this->em->getMetadataFactory()->getMetadataFor(UploadedFileEntity::class));
//    }

    public function save(UploadedFileEntity $uploadedFile)
    {
        $this->_em->persist($uploadedFile);
        $this->_em->flush();
    }

    public function findForUser(User $user)
    {
        return $this->findOneBy([
            "user" => $user
        ], [
            "createdAt" => 'DESC'
        ]);
    }

    public function findAllForUserQuery(User $user)
    {
        return $this->createQueryBuilder('f')
            ->select('PARTIAL f.{id}')
            ->where('f.user = :user')
            ->setParameter('user', $user);
    }
}
