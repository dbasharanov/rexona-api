<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;

class UserRepository extends \Doctrine\ORM\EntityRepository
{

    public function save(User $user)
    {
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findByEmail($email)
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function findOneByFacebookIdOREmail($facebookId, $email)
    {
        return $this->_em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.facebookId = :facebookId OR u.email = :email')
            ->setParameters([
                'facebookId' => $facebookId,
                'email' => $email
            ])
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }
}
