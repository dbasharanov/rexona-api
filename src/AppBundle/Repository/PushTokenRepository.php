<?php

namespace AppBundle\Repository;

use AppBundle\Entity\PushToken;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityManager;

class PushTokenRepository extends EntityRepository implements PushTokenRepositoryInterface
{
//    /**
//     * @var EntityManager
//     */
//    private $em;
//
//    /**
//     * @var LoggerInterface
//     */
//    protected $logger;
//
//    public function __construct($logger)
//    {
//        $this->logger = $logger;
//    }

    public function save(PushToken $pushToken)
    {
        $this->_em->persist($pushToken);
        $this->_em->flush();
    }

    public function deactivateTokenForOtherUsers(PushToken $pushToken)
    {
        // and let's disable all the other active tokens that aren't attached to this user
        $pushTokenTable = $this->_em->getClassMetadata(PushToken::class)->getTableName();
        $queryBuilder = $this->_em->getConnection()->createQueryBuilder();
        $queryBuilder
            ->update($pushTokenTable)
            ->set('is_active', '0')
            ->where(
                $queryBuilder->expr()->andX(
                    $queryBuilder->expr()->eq('is_active', '?'),
                    $queryBuilder->expr()->eq('token', '?'),
                    $queryBuilder->expr()->eq('type', '?'),
                    $queryBuilder->expr()->neq('user_id', '?')
                )
            )
            ->setParameter(0, 1)
            ->setParameter(1, $pushToken->getToken())
            ->setParameter(2, $pushToken->getType())
            ->setParameter(3, $pushToken->getUser()->getId())
            ->execute();
    }

    public function findByToken($token)
    {
        return $this->_em->getRepository(PushToken::class)->findOneBy([
            "token" => $token
        ]);
    }

    public function findByUserAndToken($user, $token)
    {
        return $this->findOneBy([
            "user" => $user,
            "token" => $token
        ]);
    }
}
