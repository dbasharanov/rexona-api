<?php

namespace AppBundle\Repository;

use AppBundle\Entity\PushToken;
use Doctrine\ORM\EntityRepository;

class PointsRepository extends EntityRepository
{

    public function save(PushToken $pushToken)
    {
        $this->_em->persist($pushToken);
        $this->_em->flush();
    }
}
