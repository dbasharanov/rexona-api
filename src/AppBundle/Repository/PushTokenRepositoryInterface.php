<?php

namespace AppBundle\Repository;

use AppBundle\Entity\PushToken;
use AppBundle\Entity\User;

interface PushTokenRepositoryInterface
{
    /**
     * @param PushToken $pushToken
     */
    public function save(PushToken $pushToken);

    /**
     * @param PushToken $pushToken
     */
    public function deactivateTokenForOtherUsers(PushToken $pushToken);

    /**
     * @param string $token
     * @return PushToken
     */
    public function findByToken($token);

    /**
     * @param User $user
     * @param string $token
     * @return PushToken
     */
    public function findByUserAndToken($user, $token);
}