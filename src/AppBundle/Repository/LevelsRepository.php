<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Levels;
use AppBundle\Entity\PushToken;
use Doctrine\ORM\EntityRepository;

class LevelsRepository extends EntityRepository
{

    public function save(PushToken $pushToken)
    {
        $this->_em->persist($pushToken);
        $this->_em->flush();
    }

    public function getNextLevel(Levels $level)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.id > :id')
            ->setParameter(':id', $level->getId())
            ->orderBy('u.id', 'ASC')
            ->setFirstResult(0)
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getPrevLevels(Levels $level)
    {
        $qb = $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.id < :id')
            ->setParameter(':id', $level->getId())
            ->orderBy('u.id', 'DESC')
            ->setFirstResult(0);

        return $qb->getQuery()->getResult();
    }
}
