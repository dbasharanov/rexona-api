<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineExtensions\Timestampable\Timestampable;
use JMS\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PushTokenRepository")
 * @UniqueEntity(
 *     fields={"user", "type", "token"},
 *     message="This token has already been stored."
 * )
 */
class PushToken
{
    use Timestampable;

    const TYPE = [
        'ios' => 0,
        'android' => 1
    ];

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="pushTokens")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"List"})
     */
    private $type;

    /**
     * @ORM\Column(type="text")
     * @Groups({"List"})
     */
    private $token;

    /**
     * @ORM\Column(type="boolean", options={"default" = TRUE})
     */
    private $isActive = true;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @param boolean $isActive
     * @return $this
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }
}