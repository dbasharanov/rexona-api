<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineExtensions\Timestampable\Timestampable;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UploadedFileRepository")
 */
class UploadedFile
{
    use Timestampable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="User")
     */
    protected $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    public function __construct(User $user, string $path)
    {
        $this->user = $user;
        $this->path = $path;
    }

    public function __toString()
    {
        return (string)$this->getPath();   // TODO: Implement __toString() method.
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return UploadedFile
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User|null $user
     *
     * @return UploadedFile
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
