<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Groups;
use DoctrineExtensions\Timestampable\Timestampable;
use Patchwork\Utf8;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @UniqueEntity("email")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    use Timestampable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @ORM\Column(type="string", length=191, nullable=true, unique=true) */
    private $facebookId;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=191, unique=true, nullable=true)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    private $email;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $password;

    /** @ORM\Column(type="boolean", options={"default" = true}) */
    private $isVerified = true;

    /** @ORM\Column(type="boolean", options={"default" = false}) */
    private $isEmailVerified = false;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $verificationToken;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $passwordResetToken;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $wsToken;

    /** @ORM\OneToMany(targetEntity="AppBundle\Entity\PushToken", mappedBy="user") */
    private $pushTokens;

    /** @ORM\OneToMany(targetEntity="AppBundle\Entity\Points", mappedBy="user") */
    private $points;

    /** @ORM\ManyToOne(targetEntity="AppBundle\Entity\Levels") */
    private $level;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     * @Groups({"PushSettings"})
     */
    private $pushSettings1 = true;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     * @Groups({"PushSettings"})
     */
    private $pushSettings2 = true;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     * @Groups({"PushSettings"})
     */
    private $pushSettings3 = true;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     * @Groups({"PushSettings"})
     */
    private $emailSettings1 = true;

    /**
     * @ORM\Column(type="boolean", options={"default" = true})
     * @Groups({"PushSettings"})
     */
    private $emailSettings2 = true;

    /** @ORM\Column(type="string", length=255, nullable=true) */
    private $language;

    /** @ORM\Column(type="text", nullable=true) */
    private $fbData;

    /**
     * @ORM\OneToOne(targetEntity="UploadedFile", mappedBy="user")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $photo;

    /**
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    public function __construct()
    {
        $this->generateVerificationToken();
        $this->generateWsToken();

        $this->pushTokens = new ArrayCollection();
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return (string)$this->getEmail();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getFacebookId()
    {
        return $this->facebookId;
    }

    /**
     * @param mixed $facebookId
     * @return $this
     */
    public function setFacebookId($facebookId)
    {
        $this->facebookId = $facebookId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function hasEmail()
    {
        return Utf8::strlen($this->email) > 0;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function hasPassword()
    {
        return Utf8::strlen($this->password) > 0;
    }

    /**
     * @return bool
     */
    public function getIsEmailVerified()
    {
        return $this->isEmailVerified;
    }

    /**
     * @param bool $isEmailVerified
     * @return $this
     */
    public function setIsEmailVerified($isEmailVerified)
    {
        $this->isEmailVerified = $isEmailVerified;
        return $this;
    }

    /**
     * @return bool
     */
    public function getIsVerified()
    {
        return $this->isVerified;
    }

    /**
     * @param bool $isVerified
     * @return $this
     */
    public function setIsVerified($isVerified)
    {
        $this->isVerified = $isVerified;
        return $this;
    }

    /**
     * @return string
     */
    public function getVerificationToken()
    {
        return $this->verificationToken;
    }

    /**
     * @param string $verificationToken
     * @return $this
     */
    public function setVerificationToken($verificationToken)
    {
        $this->verificationToken = $verificationToken;
        return $this;
    }

    public function generateVerificationToken()
    {
        $this->verificationToken = bin2hex(random_bytes(32));
    }

    /**
     * @return string
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * @param string $passwordResetToken
     * @return $this
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;
        return $this;
    }

    public function generatePasswordResetToken()
    {
        $this->passwordResetToken = bin2hex(random_bytes(32));
    }

    /**
     * @return string
     */
    public function getWsToken()
    {
        return $this->wsToken;
    }

    /**
     * @param string $wsToken
     * @return $this
     */
    public function setWsToken($wsToken)
    {
        $this->wsToken = $wsToken;
        return $this;
    }

    public function generateWsToken()
    {
        $this->wsToken = bin2hex(random_bytes(16));
    }

    /**
     * @return PushToken[]|Collection
     */
    public function getPushTokens()
    {
        return $this->pushTokens;
    }

    /**
     * @return bool
     */
    public function getPushSettings1()
    {
        return $this->pushSettings1;
    }

    /**
     * @param bool $pushSettings1
     * @return $this
     */
    public function setPushSettings1($pushSettings1)
    {
        $this->pushSettings1 = $pushSettings1;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPushSettings2()
    {
        return $this->pushSettings2;
    }

    /**
     * @param bool $pushSettings2
     * @return $this
     */
    public function setPushSettings2($pushSettings2)
    {
        $this->pushSettings2 = $pushSettings2;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPushSettings3()
    {
        return $this->pushSettings3;
    }

    /**
     * @param bool $pushSettings3
     * @return $this
     */
    public function setPushSettings3($pushSettings3)
    {
        $this->pushSettings3 = $pushSettings3;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEmailSettings1()
    {
        return $this->emailSettings1;
    }

    /**
     * @param bool $emailSettings1
     * @return $this
     */
    public function setEmailSettings1($emailSettings1)
    {
        $this->emailSettings1 = $emailSettings1;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEmailSettings2()
    {
        return $this->emailSettings2;
    }

    /**
     * @param bool $emailSettings2
     * @return $this
     */
    public function setEmailSettings2($emailSettings2)
    {
        $this->emailSettings2 = $emailSettings2;
        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     * @return $this
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return string
     */
    public function getFbData()
    {
        return $this->fbData;
    }

    /**
     * @param string $fbData
     * @return $this
     */
    public function setFbData($fbData)
    {
        $this->fbData = $fbData;
        return $this;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getRoles()
    {
        $roles = ['ROLE_USER_UNVERIFIED'];
        if ($this->getIsVerified()) {
            $roles = ['ROLE_USER'];
        }

        return $roles;
    }

    public function getSalt()
    {

    }

    public function eraseCredentials()
    {

    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return User
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add pushToken.
     *
     * @param \AppBundle\Entity\PushToken $pushToken
     *
     * @return User
     */
    public function addPushToken(\AppBundle\Entity\PushToken $pushToken)
    {
        $this->pushTokens[] = $pushToken;

        return $this;
    }

    /**
     * Remove pushToken.
     *
     * @param \AppBundle\Entity\PushToken $pushToken
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePushToken(\AppBundle\Entity\PushToken $pushToken)
    {
        return $this->pushTokens->removeElement($pushToken);
    }

    /**
     * Set photo.
     *
     * @param \AppBundle\Entity\UploadedFile|null $photo
     *
     * @return User
     */
    public function setPhoto(\AppBundle\Entity\UploadedFile $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo.
     *
     * @return \AppBundle\Entity\UploadedFile|null
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Add point.
     *
     * @param \AppBundle\Entity\Points $point
     *
     * @return User
     */
    public function addPoint(\AppBundle\Entity\Points $point)
    {
        $this->points[] = $point;

        return $this;
    }

    /**
     * Remove point.
     *
     * @param \AppBundle\Entity\Points $point
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePoint(\AppBundle\Entity\Points $point)
    {
        return $this->points->removeElement($point);
    }

    /**
     * Get points.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set level.
     *
     * @param \AppBundle\Entity\Levels|null $level
     *
     * @return User
     */
    public function setLevel(\AppBundle\Entity\Levels $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level.
     *
     * @return \AppBundle\Entity\Levels|null
     */
    public function getLevel()
    {
        return $this->level;
    }
}
