<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DoctrineExtensions\Timestampable\Timestampable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LevelsRepository")
 */
class Levels
{
    use Timestampable;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="string", length=255)
     */
    private $mountain;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="integer")
     */
    private $points;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="integer")
     */
    private $height;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", options={"default" = false})
     */
    private $expert = false;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Levels
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set mountain.
     *
     * @param string $mountain
     *
     * @return Levels
     */
    public function setMountain($mountain)
    {
        $this->mountain = $mountain;

        return $this;
    }

    /**
     * Get mountain.
     *
     * @return string
     */
    public function getMountain()
    {
        return $this->mountain;
    }

    /**
     * Set points.
     *
     * @param int $points
     *
     * @return Levels
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Get points.
     *
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Levels
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set expert.
     *
     * @param bool $expert
     *
     * @return Levels
     */
    public function setExpert($expert)
    {
        $this->expert = $expert;

        return $this;
    }

    /**
     * Get expert.
     *
     * @return bool
     */
    public function getExpert()
    {
        return $this->expert;
    }
}
