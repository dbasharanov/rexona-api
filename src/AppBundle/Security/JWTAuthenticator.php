<?php

namespace AppBundle\Security;

use AppBundle\Entity\User;
use AppBundle\Service\JWT;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class JWTAuthenticator extends AbstractGuardAuthenticator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var JWT
     */
    private $jwt;

    public function __construct(EntityManager $entityManager, JWT $jwt)
    {
        $this->userRepository = $entityManager->getRepository(User::class);
        $this->jwt = $jwt;
    }

    public function getCredentials(Request $request)
    {
        if (!preg_match('/^Bearer (.*)/', $request->headers->get('Authorization'), $matches)) {
            return null;
        }

        return ['token' => $matches[1]];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = $credentials['token'];

        try {
            $decoded = $this->jwt->decode($token);
        } catch (\Exception $e) {
            return null;
        }

        return $this->userRepository->findOneBy(['id' => $decoded['uid']]);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = ['message' => strtr($exception->getMessageKey(), $exception->getMessageData())];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = ['message' => 'Authentication required.'];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
