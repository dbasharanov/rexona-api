<?php

namespace AppBundle\Service;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\ConstraintViolation;

class FormErrorSerializer
{
    /**
     * @param FormInterface $form
     * @return array
     */
    public static function getErrors(FormInterface $form)
    {
        return static::extractErrors($form->getErrors(true, false));
    }

    /**
     * @param FormErrorIterator $formErrorIterator
     * @param string $prefix
     * @return array
     */
    private static function extractErrors(FormErrorIterator $formErrorIterator, $prefix = "")
    {
        $form = $formErrorIterator->getForm();

        if (null === $form->getParent()) {
            $name = "";
        } else {
            $name = strlen($prefix) ? "{$prefix}[{$form->getName()}]" : $form->getName();
        }
        $result = [];

        foreach ($formErrorIterator as $error) {

            if ($error instanceof FormError) {

                $errorType = null;

                $cause = $error->getCause();

                if ($cause instanceof ConstraintViolation) {
                    $constraint = $cause->getConstraint();
                    try {
                        $errorType = $constraint->getErrorName($cause->getCode());
                    } catch (\InvalidArgumentException $e) {

                        if ($constraint instanceof UniqueEntity) {
                            $errorType = 'NOT_UNIQUE_ERROR';
                        }
                    }
                }

                $result[] = [
                    'path' => $name,
                    'type' => $errorType,
                    'message' => $error->getMessage(),
                ];
            } elseif ($error instanceof FormErrorIterator) {
                $result = array_merge($result, self::extractErrors($error, $name));
            }
        }

        return $result;
    }
}