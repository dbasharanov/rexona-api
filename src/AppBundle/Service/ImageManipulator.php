<?php

namespace AppBundle\Service;

use AppBundle\Domain\Image\Filter;
use AppBundle\Domain\Image\FilterFactoryInterface;
use League\Glide\Urls\UrlBuilder;

class ImageManipulator
{
    /**
     * @var UrlBuilder
     */
    private $urlBuilder;

    /**
     * @var FilterFactoryInterface
     */
    private $filterFactory;

    public function __construct(UrlBuilder $urlBuilder, FilterFactoryInterface $filterFactory)
    {
        $this->urlBuilder = $urlBuilder;
        $this->filterFactory = $filterFactory;
    }

    /**
     * Returns a URL with image and its manipulating parameters
     * @param $url
     * @param $filterName
     * @param array $cropArray
     * @return string
     */
    public function generatePhotoURL($url, $filterName, $cropArray = [])
    {
        $filter = $this->filterFactory->create($filterName);

        // Apply the crop parameters first if existing
        if (isset($cropArray) && isset($cropArray['x']) && isset($cropArray['y']) && isset($cropArray['w']) && isset($cropArray['h'])) {
            $filter->setCrop($cropArray['w'], $cropArray['h'], $cropArray['x'], $cropArray['y']);
        }

        return $this->urlBuilder->getUrl($url, $this->getFilterParams($filter));
    }

    /**
     * @param Filter $filter
     * @return array
     */
    private function getFilterParams(Filter $filter)
    {
        $filterOptions = [
            'crop' => 'getCrop',
            'w' => 'getWidth',
            'h' => 'getHeight',
            'fit' => 'getFit',
            'blur' => 'getBlur',
            'bggen' => 'getBackgroundGenerator',
            'overlay' => 'getOverlay'
        ];

        $params = [];

        foreach ($filterOptions as $function => $accessor) {
            if ($filter->$accessor()) {
                $params[$function] = $filter->$accessor();
            }
        }

        return $params;
    }
}
