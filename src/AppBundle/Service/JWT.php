<?php

namespace AppBundle\Service;

class JWT
{
    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $alg;

    public function __construct(string $key, string $alg)
    {
        $this->key = $key;
        $this->alg = $alg;
    }

    public function encode(array $payload): string
    {
        return \Firebase\JWT\JWT::encode($payload, $this->key, $this->alg);
    }

    public function decode(string $token): array
    {
        return (array)\Firebase\JWT\JWT::decode($token, $this->key, [$this->alg]);
    }
}