<?php

namespace AppBundle\DataTransformer;

use AppBundle\Domain\Image\FilterFactoryInterface;
use AppBundle\Entity\Candidate;
use AppBundle\Entity\User;
use AppBundle\Entity\Venue;
use AppBundle\Service\ImageManipulator;
use AppBundle\Service\JWT;
use League\Fractal\TransformerAbstract;

class UserDataTransformer extends TransformerAbstract
{
    /** @var JWT */
    private $jwt;

    /**
     * @var ImageManipulator
     */
    private $imageManipulator;

    public function __construct(JWT $jwt, ImageManipulator $imageManipulator)
    {
        $this->jwt = $jwt;
        $this->imageManipulator = $imageManipulator;
    }

    public function transform(User $user)
    {
        $result = [
            'id' => $user->getId(),
            'createdAt' => $user->getCreatedAt()->getTimestamp(),
            'token' => $this->jwt->encode(['uid' => $user->getId(), 'time' => time()]),
            'wsToken' => $user->getWsToken(),
            'isVerified' => $user->getIsVerified(),
            'email' => $user->getEmail(),
            'name' => $user->getName(),
            'hasEmail' => $user->hasEmail(),
            'hasPassword' => $user->hasPassword(),
            'image' => null,
        ];

        $photoURL = null;
//        dump($user);
//        dump($user->getPhoto()->getPath());
//        exit;
        if ($user->getPhoto() && $user->getPhoto()->getPath()) {
            $photoURL = $this->imageManipulator->generatePhotoURL(
                $user->getPhoto()->getPath(),
                FilterFactoryInterface::PLAIN_FILTER,
                []
            );
        }

        $result["points"] = $user->getPoints();

        $result['image'] = $photoURL;
        return $result;
    }
}