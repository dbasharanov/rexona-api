<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ProductsController
 *
 * @Route("/", defaults={"_locale"="en"})
 */
class ProductsController extends Controller
{
    /**
     * @Route("/products", name="app_products")
     * @Template()
     */
    public function indexAction(Request $request, EntityManager $entityManager)
    {
        return array(
            'class' => 'dark-blue'
        );
    }

    /**
     * @Route("/products/{slug}", name="app_product_show")
     * @Template()
     */
    public function showAction($slug)
    {   
        $image_path = "bundles/app/images/temp/".$slug.'_wall.jpg';
        return array(
            'class' => 'dark-blue',
            'image_path' => $image_path
        ); 
    }

}