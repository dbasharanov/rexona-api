<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use V3labs\AdminBundle\Entity\Award;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DefaultController
 *
 * @Route("/", defaults={"_locale"="en"})
 */
class DefaultController extends Controller
{
    /**
     * @Route("", name="app_homepage")
     * @Template()
     */
    public function indexAction(Request $request, EntityManager $entityManager)
    {

        $endorsers = $this->getDoctrine()
        ->getRepository('V3labsAdminBundle:Endorser')
        ->findAll();

        return array(
            'class' => 'topology dark-blue homepage',
            'endorsers' => $endorsers,

        );
      
    }

    /**
     * @Route("/awards", name="app_awards")
     * @Template()
     */
    public function awardsAction()
    { 
        $awards = $this->getDoctrine()
            ->getRepository('V3labsAdminBundle:Award')
            ->findAll();

        $award_labels = array('Първа', 'Втора', 'Трета');
        
        return array(
            'class' => 'dark-blue',
            'awards' => $awards,
            'award_labels' => $award_labels
        );
    }

    /**
     * @Route("/about", name="app_about")
     * @Template()
     */
    public function aboutAction()
    { 
    }

    /**
     * @Route("/tos", name="app_tos")
     * @Template()
     */
    public function tosAction()
    { 
    }

    /**
     * @Route("/confidentiality-policy", name="app_confidentiality_policy")
     * @Template()
     */
    public function confidentialityPolicyAction()
    { 
    }

}
