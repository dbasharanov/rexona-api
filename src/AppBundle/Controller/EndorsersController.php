<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use V3labs\AdminBundle\Entity\Endorser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EventsController
 *
 * @Route("/", defaults={"_locale"="en"})
 */
class EndorsersController extends Controller
{
    /**
     * @Route("/endorsers", name="app_endorsers")
     * @Template()
     */
    public function indexAction(Request $request, EntityManager $entityManager)
    {
        $endorsers = $this->getDoctrine()
                          ->getRepository('V3labsAdminBundle:Endorser')
                          ->findAll();

        return array(
            'class' => 'topology dark-blue',
            'endorsers' => $endorsers
        );
    }

    /**
     * @Route("/endorsers/{slug}", name="app_endorser_show")
     * @Template()
     */
    public function showAction($slug)
    {   
        $endorser = $this->getDoctrine()
                         ->getRepository('V3labsAdminBundle:Endorser')
                         ->findBySlug($slug)[0];

        $endorsers = $this->getDoctrine()
                          ->getRepository('V3labsAdminBundle:Endorser')
                          ->findAll();


        return array(
            'endorser' => $endorser,
            'endorsers' => $endorsers
        );
    }

}
