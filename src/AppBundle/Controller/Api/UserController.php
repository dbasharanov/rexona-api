<?php

namespace AppBundle\Controller\Api;

use AppBundle\Entity\Levels;
use AppBundle\Entity\Points;
use AppBundle\Entity\PushToken;
use AppBundle\Entity\UploadedFile;
use AppBundle\Entity\User;
use AppBundle\Form\PointsType;
use AppBundle\Form\PushTokenType;
use AppBundle\Form\UserCreateType;
use AppBundle\Service\FormErrorSerializer;
use AppBundle\Service\JWT;
use Doctrine\ORM\EntityManager;
use Facebook\Facebook;
use JMS\Serializer\SerializationContext;
use League\Fractal\Resource\Item;
use Patchwork\Utf8;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package AppBundle\Controller
 * @Route("/user")
 */
class UserController extends Controller
{
    /**
     * @Route("/register")
     * @Method("POST")
     */
    public function registerAction(Request $request, EntityManager $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $userRepository = $entityManager->getRepository(User::class);
        $jwt = $this->get(JWT::class);

        $user = new User();
        $form = $this->createForm(UserCreateType::class, $user);
        $form->handleRequest($request);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return new JsonResponse(
                ['validationErrors' => FormErrorSerializer::getErrors($form)],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }


        $password = $encoder->encodePassword($user, $form->get('password')->getData());
        $user->setPassword($password);

        $userRepository->save($user);
        $fractal = $this->get('app.fractal');
        $transformer = $this->get('app.transformer.user_data');

        $data = $fractal->createData(new Item($user, $transformer))->toArray();

        return new JsonResponse([
            'data' => $data
        ]);
    }

    /**
     * @Route("/login")
     * @Method("POST")
     */
    public function loginAction(Request $request, EntityManager $entityManager, UserPasswordEncoderInterface $encoder)
    {
        $userRepository = $entityManager->getRepository(User::class);

        $fractal = $this->get('app.fractal');
        $transformer = $this->get('app.transformer.user_data');

        $email = $request->get('email');
        $password = $request->get('password');


        /** @var User $user */
        $user = $userRepository->findByEmail($email);
        if (null === $user || !$encoder->isPasswordValid($user, $password)) {
            return new JsonResponse(['message' => 'Incorrect email or password.'], Response::HTTP_UNAUTHORIZED);
        }

        $data = $fractal->createData(new Item($user, $transformer))->toArray();

        return new JsonResponse([
            'data' => $data
        ]);
    }

    /**
     * @Route("/facebook-login")
     * @Method("POST")
     */
    public function facebookLoginAction(Request $request, EntityManager $entityManager)
    {
        $userRepository = $entityManager->getRepository(User::class);
        $uploadedFileRepository = $entityManager->getRepository(UploadedFile::class);
        $fractal = $this->get('app.fractal');
        $transformer = $this->get('app.transformer.user_data');

        $fb = new Facebook([
            'app_id' => $this->getParameter('facebook.app_id'),
            'app_secret' => $this->getParameter('facebook.app_secret'),
            'default_graph_version' => 'v3.2',
        ]);

        $fb->setDefaultAccessToken($request->get('access_token'));

        try {
//            $response = $fb->get(
//                '/me?fields=id,first_name,last_name,email,gender,location,picture.width(800).height(800),birthday,hometown'
//            );
            $response = $fb->get(
                '/me?fields=id,first_name,last_name,email,gender,picture.width(800).height(800)'
            );
            $userNode = $response->getGraphUser();
        } catch (FacebookResponseException $e) {
            return new JsonResponse(['message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (FacebookSDKException $e) {
            return new JsonResponse(['message' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** @var User $user */
        $user = $userRepository->findOneByFacebookIdOREmail($userNode->getId(), $userNode->getEmail());

        if (null !== $user) {
            $user->setFacebookId($userNode->getId());

            if (Utf8::strtolower($userNode->getEmail()) == Utf8::strtolower($user->getEmail())) {
                $user->setIsVerified(true);
                $user->setIsEmailVerified(true);
            }
        }

        if (null == $user) {
            $user = (new User())
                ->setFacebookId($userNode->getId())
                ->setEmail($userNode->getEmail())
                ->setIsVerified(true)
                ->setIsEmailVerified(true);
            $fbData = null;
            if (null !== $userNode->getFirstName()) {
                $fbData['firstName'] = $userNode->getFirstName();
                $user->setName($userNode->getFirstName());
            }
            if (null !== $userNode->getLastName()) {
                $fbData['lastName'] = $userNode->getLastName();
                $user->setName($user->getName() . ' ' . $userNode->getLastName());
            }
            if (null !== $userNode->getBirthday()) {
                $fbData['dateOfBirth'] = $userNode->getBirthday()->format('Y-m-d');
            }
            if (null !== $userNode->getPicture()->getUrl()) {

                $filesystem = $this->get('app.glide.filesystem');
                $sourcePrefix = $this->getParameter('glide.source_path_prefix');

                $url = $userNode->getPicture()->getUrl();

                $urlImagePathInfo = pathinfo($url);
                if (!array_key_exists('extension', $urlImagePathInfo)) {
                    $image = file_get_contents($url);
                    $urlImagePathInfo = getimagesizefromstring($image);
                    if (is_array($urlImagePathInfo)) {
                        switch ($urlImagePathInfo[2]) {
                            case IMAGETYPE_GIF:
                                $urlImageExtension = 'gif';
                                break;
                            case IMAGETYPE_PNG:
                                $urlImageExtension = 'png';
                                break;
                            default:
                                $urlImageExtension = 'jpeg';
                        }
                    }
                } else {
                    $urlImageExtension = explode("?", $urlImagePathInfo['extension'])[0];
                }

                $this->get("monolog.logger.api")->info("extension" . $urlImageExtension);

                $pathFileName = bin2hex(random_bytes(16)) . '.' . $urlImageExtension;

                $path = implode('/', [date('Y'), date('m'), $pathFileName]);

                $tempPath = $this->get('kernel')->getRootDir() . '/../var/cache/';
                $tempImagePath = $tempPath . $pathFileName;
                file_put_contents($tempImagePath, file_get_contents($url));

                $stream = fopen($tempImagePath, 'r+');
                $filesystem->writeStream(implode('/', [$sourcePrefix, $path]), $stream);
                fclose($stream);


                $userRepository->save($user);
                $uploadedFileEntity = new UploadedFile($user, $path);
                $uploadedFileRepository->save($uploadedFileEntity);

                $user->setPhoto($uploadedFileEntity);
                $userRepository->save($user);

                @unlink($tempImagePath);
            }

            if (null !== $fbData) {
                $user->setFbData(serialize($fbData));
            }
        }

        $userRepository->save($user);

        $data = $fractal->createData(new Item($user, $transformer))->toArray();
        return new JsonResponse([
            'data' => $data
        ]);
    }

    /**
     * @Route("/google-login")
     */
    public function googleLoginAction(Request $request, EntityManager $entityManager)
    {
        $userRepository = $entityManager->getRepository(User::class);
        $uploadedFileRepository = $entityManager->getRepository(UploadedFile::class);
//        $fractal = $this->get('app.fractal');
//        $transformer = $this->get('app.transformer.user_data');

        $client = new \Google_Client();
        $client->setClientId('124599450284-tkjip3usha3jems59fc4brkl0i6vck6t.apps.googleusercontent.com');
        $client->setClientSecret('kO23HqdG0IKWFQTLYBFw2LgK');
        $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/api/user/google-login');
        $client->addScope(\Google_Service_Drive::DRIVE_METADATA_READONLY);

        if ($request->query->get('code')) {
            $service = new \Google_Service_Oauth2($client);
            $code = $client->authenticate($request->query->get('code'));// to get code
            dump($code);
            $client->setAccessToken($code);// to get access token by setting of $code
            $userDetails = $service->userinfo->get();// to get user detail by using access token
            dump($userDetails);
        } else {
            $auth_url = $client->createAuthUrl();
            dump(filter_var($auth_url, FILTER_SANITIZE_URL));

        }
        exit;

        $data = $fractal->createData(new Item($user, $transformer))->toArray();
        return new JsonResponse(['data' => $data]);
    }

    /**
     * @Route("/push-token")
     * @Method("GET")
     */
    public function pushTokenIndexAction(Request $request)
    {
        $serializer = $this->get('jms_serializer');

        /** @var User $user */
        $user = $this->getUser();

        return new JsonResponse([
            'types' => [
                'ios' => PushToken::TYPE['ios'],
                'android' => PushToken::TYPE['android']
            ],
            'data' => $serializer->toArray(
                $user->getPushTokens(),
                SerializationContext::create()->setGroups(['List'])->setSerializeNull(true)
            )
        ]);
    }

    /**
     * @Route("/push-token")
     * @Method("POST")
     */
    public function pushTokenStoreAction(Request $request, EntityManager $entityManager)
    {
        $pushTokenRepository = $entityManager->getRepository(PushToken::class);
        /** @var User $user */
        $user = $this->getUser();

        $pushToken = $pushTokenRepository->findByUserAndToken($user, $request->request->get("token"));

        if (null == $pushToken) {
            $pushToken = new PushToken($user);
        }

        $form = $this->createForm(PushTokenType::class, $pushToken);
        $form->submit([
            'token' => $request->request->get('token'),
            'type' => array_key_exists($request->request->get('type'), PushToken::TYPE) ? PushToken::TYPE[$request->request->get('type')] : 0
        ]);

        if (!$form->isValid()) {
            return new JsonResponse(
                ['validationErrors' => FormErrorSerializer::getErrors($form)],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
        $pushToken->setIsActive(true);

        $pushTokenRepository->save($pushToken);
        $pushTokenRepository->deactivateTokenForOtherUsers($pushToken);

        return new JsonResponse();
    }

    /**
     * @Route("/logout")
     * @Method("POST")
     */
    public function logoutAction(Request $request, EntityManager $entityManager)
    {
        $pushTokenRepository = $entityManager->getRepository(PushToken::class);

        /** @var User $user */
        $user = $this->getUser();

        $pushToken = new PushToken($user);
        $form = $this->createForm(PushTokenType::class, $pushToken);
        $form->submit($request->request->all());

        $token = $form->get("token")->getData();
        $pushToken = $pushTokenRepository->findByToken($token);

        if (null !== $pushToken) {
            $pushToken->setIsActive(false);
            $pushTokenRepository->save($pushToken);
        }

        return new JsonResponse();
    }

    /**
     * @Route("/progress")
     * @Method("GET")
     */
    public function progressAction(EntityManager $entityManager)
    {
        /** @var  $user */
        $user = $this->getUser();

//        $pointsRepository = $entityManager->getRepository(Points::class);
//        $points = $pointsRepository->findBy(
//            ['user' => $user]
//        );

        $points = $user->getPoints();
        $totalPoints = 0;
        foreach ($points as $point) {
            $totalPoints += $point->getPoints();
        }

        $currentLevel = $user->getLevel();

        $nextLevel = null;
        if ($currentLevel) {
            $previousLevels = $entityManager->getRepository(Levels::class)->getPrevLevels($currentLevel);
            $nextLevel = $entityManager->getRepository(Levels::class)->getNextLevel($currentLevel);
        }

        $totalLevelsPoints = 0;
        foreach ($previousLevels as $level) {
            $totalLevelsPoints += $level->getPoints();
        }

        dump($currentLevel);
        dump($nextLevel);
        dump($totalPoints);
        dump($totalPoints - $totalLevelsPoints);
        exit;

        return new JsonResponse([
            'currentLevel' => $user->getLevel(),
            'nextLevel' => $nextLevel,
            "totalPoints" => $points,

        ]);
    }

    /**
     * @Route("/progress")
     * @Method("POST")
     */
    public function setProgressAction(Request $request, EntityManager $entityManager)
    {
        /** @var  $user */
        $user = $this->getUser();

        $points = $entityManager->getRepository(Points::class)->findOneBy(['user' => $user, 'date' => new \DateTime()]);

        if (!$points) {
            $points = new Points($user);
        }
        $form = $this->createForm(PointsType::class, $points);
        $form->submit($request->request->all());

        if (!$form->isValid()) {
            return new JsonResponse(
                ['validationErrors' => FormErrorSerializer::getErrors($form)],
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $token = $form->get("token")->getData();

        $points = $user->getPoints();
        $totalPoints = 0;
        foreach ($points as $point) {
            $totalPoints += $point->getPoints();
        }

        $currentLevel = $user->getLevel();

        $nextLevel = null;
        if ($currentLevel) {
            $previousLevels = $entityManager->getRepository(Levels::class)->getPrevLevels($currentLevel);
            $nextLevel = $entityManager->getRepository(Levels::class)->getNextLevel($currentLevel);
        }

        $totalLevelsPoints = 0;
        foreach ($previousLevels as $level) {
            $totalLevelsPoints += $level->getPoints();
        }

        dump($currentLevel);
        dump($nextLevel);
        dump($totalPoints);
        dump($totalPoints - $totalLevelsPoints);
        exit;

        return new JsonResponse([
            'currentLevel' => $user->getLevel(),
            'nextLevel' => $nextLevel,
            "totalPoints" => $points,

        ]);
    }

}