<?php

namespace AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class EventsController
 *
 * @Route("/", defaults={"_locale"="en"})
 */
class EventsController extends Controller
{
    /**
     * @Route("/events", name="app_events")
     * @Template()
     */
    public function indexAction(Request $request, EntityManager $entityManager)
    {
        return array(
            'class' => 'topology dark-blue'
        );
    }

    /**
     * @Route("/events/{slug}", name="app_event_show")
     * @Template()
     */
    public function showAction()
    { 
    }

}
