<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/user')) {
            // app_api_user_register
            if ('/user/register' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_app_api_user_register;
                }

                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_api_user_register', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::registerAction',  '_route' => 'app_api_user_register',);
            }
            not_app_api_user_register:

            // app_api_user_login
            if ('/user/login' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_app_api_user_login;
                }

                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_api_user_login', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::loginAction',  '_route' => 'app_api_user_login',);
            }
            not_app_api_user_login:

            // app_api_user_logout
            if ('/user/logout' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_app_api_user_logout;
                }

                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_api_user_logout', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::logoutAction',  '_route' => 'app_api_user_logout',);
            }
            not_app_api_user_logout:

            // app_api_user_facebooklogin
            if ('/user/facebook-login' === $pathinfo) {
                if ('POST' !== $canonicalMethod) {
                    $allow[] = 'POST';
                    goto not_app_api_user_facebooklogin;
                }

                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_api_user_facebooklogin', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::facebookLoginAction',  '_route' => 'app_api_user_facebooklogin',);
            }
            not_app_api_user_facebooklogin:

            // app_api_user_googlelogin
            if ('/user/google-login' === $pathinfo) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_api_user_googlelogin', key($requiredSchemes));
                }

                return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::googleLoginAction',  '_route' => 'app_api_user_googlelogin',);
            }

            if (0 === strpos($pathinfo, '/user/push-token')) {
                // app_api_user_pushtokenindex
                if ('/user/push-token' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_app_api_user_pushtokenindex;
                    }

                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$scheme])) {
                        return $this->redirect($rawPathinfo, 'app_api_user_pushtokenindex', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::pushTokenIndexAction',  '_route' => 'app_api_user_pushtokenindex',);
                }
                not_app_api_user_pushtokenindex:

                // app_api_user_pushtokenstore
                if ('/user/push-token' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_app_api_user_pushtokenstore;
                    }

                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$scheme])) {
                        return $this->redirect($rawPathinfo, 'app_api_user_pushtokenstore', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::pushTokenStoreAction',  '_route' => 'app_api_user_pushtokenstore',);
                }
                not_app_api_user_pushtokenstore:

            }

            elseif (0 === strpos($pathinfo, '/user/progress')) {
                // app_api_user_progress
                if ('/user/progress' === $pathinfo) {
                    if ('GET' !== $canonicalMethod) {
                        $allow[] = 'GET';
                        goto not_app_api_user_progress;
                    }

                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$scheme])) {
                        return $this->redirect($rawPathinfo, 'app_api_user_progress', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::progressAction',  '_route' => 'app_api_user_progress',);
                }
                not_app_api_user_progress:

                // app_api_user_setprogress
                if ('/user/progress' === $pathinfo) {
                    if ('POST' !== $canonicalMethod) {
                        $allow[] = 'POST';
                        goto not_app_api_user_setprogress;
                    }

                    $requiredSchemes = array (  'http' => 0,);
                    if (!isset($requiredSchemes[$scheme])) {
                        return $this->redirect($rawPathinfo, 'app_api_user_setprogress', key($requiredSchemes));
                    }

                    return array (  '_controller' => 'AppBundle\\Controller\\Api\\UserController::setProgressAction',  '_route' => 'app_api_user_setprogress',);
                }
                not_app_api_user_setprogress:

            }

        }

        // app_homepage
        if ('' === $trimmedPathinfo) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($rawPathinfo.'/', 'app_homepage');
            }

            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$scheme])) {
                return $this->redirect($rawPathinfo, 'app_homepage', key($requiredSchemes));
            }

            return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'app_homepage',);
        }

        if (0 === strpos($pathinfo, '/a')) {
            // app_awards
            if ('/awards' === $pathinfo) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_awards', key($requiredSchemes));
                }

                return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\DefaultController::awardsAction',  '_route' => 'app_awards',);
            }

            // app_about
            if ('/about' === $pathinfo) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_about', key($requiredSchemes));
                }

                return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\DefaultController::aboutAction',  '_route' => 'app_about',);
            }

            if (0 === strpos($pathinfo, '/admin')) {
                if (0 === strpos($pathinfo, '/admin/awards')) {
                    // admin_awards
                    if ('/admin/awards' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\AwardsController::indexAction',  '_route' => 'admin_awards',);
                    }

                    // admin_award_new
                    if ('/admin/awards/new' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\AwardsController::newAction',  '_route' => 'admin_award_new',);
                    }

                    // admin_award_edit
                    if (0 === strpos($pathinfo, '/admin/awards/edit') && preg_match('#^/admin/awards/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_award_edit')), array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\AwardsController::editAction',));
                    }

                    // admin_award_delete
                    if ('/admin/awards/delete' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\AwardsController::deleteAction',  '_route' => 'admin_award_delete',);
                    }

                }

                // _admin_home
                if ('/admin' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($rawPathinfo.'/', '_admin_home');
                    }

                    return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\DefaultController::indexAction',  '_route' => '_admin_home',);
                }

                // v3labs_admin_default_install
                if ('/admin/install' === $pathinfo) {
                    return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\DefaultController::installAction',  '_route' => 'v3labs_admin_default_install',);
                }

                if (0 === strpos($pathinfo, '/admin/endorsers')) {
                    // admin_endorsers
                    if ('/admin/endorsers' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\EndorsersController::indexAction',  '_route' => 'admin_endorsers',);
                    }

                    // admin_endorser_new
                    if ('/admin/endorsers/new' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\EndorsersController::newAction',  '_route' => 'admin_endorser_new',);
                    }

                    // admin_endorser_edit
                    if (0 === strpos($pathinfo, '/admin/endorsers/edit') && preg_match('#^/admin/endorsers/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_endorser_edit')), array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\EndorsersController::editAction',));
                    }

                    // admin_endorser_delete
                    if ('/admin/endorsers/delete' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\EndorsersController::deleteAction',  '_route' => 'admin_endorser_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/file-manager')) {
                    // admin_fileuploadhandler
                    if ('/admin/file-manager/fileuploadhandler' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::fileUploadHandler',  '_route' => 'admin_fileuploadhandler',);
                    }

                    // admin_getfile
                    if (0 === strpos($pathinfo, '/admin/file-manager/getfile') && preg_match('#^/admin/file\\-manager/getfile/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_getfile')), array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::getFile',));
                    }

                    if (0 === strpos($pathinfo, '/admin/file-manager/list-files')) {
                        // admin_file_manager_list_files
                        if ('/admin/file-manager/list-files' === $pathinfo) {
                            return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::listFiles',  '_route' => 'admin_file_manager_list_files',);
                        }

                        // admin_file_manager_list_files_keyword
                        if (0 === strpos($pathinfo, '/admin/file-manager/list-files-keyword') && preg_match('#^/admin/file\\-manager/list\\-files\\-keyword/(?P<keyword>[^/]++)$#s', $pathinfo, $matches)) {
                            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_file_manager_list_files_keyword')), array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::listKeywordFiles',));
                        }

                    }

                    // admin_file_manager_new_dir
                    if ('/admin/file-manager/new-dir' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::newDir',  '_route' => 'admin_file_manager_new_dir',);
                    }

                    // admin_file_manager_upload
                    if ('/admin/file-manager/upload' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::uploadAction',  '_route' => 'admin_file_manager_upload',);
                    }

                    // admin_file_manager_delete
                    if ('/admin/file-manager/delete' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\FileManagerController::deleteAction',  '_route' => 'admin_file_manager_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/users')) {
                    // admin_users
                    if ('/admin/users' === $trimmedPathinfo) {
                        if (substr($pathinfo, -1) !== '/') {
                            return $this->redirect($rawPathinfo.'/', 'admin_users');
                        }

                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserController::indexAction',  '_route' => 'admin_users',);
                    }

                    // admin_user_new
                    if ('/admin/users/new' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserController::newAction',  '_route' => 'admin_user_new',);
                    }

                    // admin_user_edit
                    if (0 === strpos($pathinfo, '/admin/users/edit') && preg_match('#^/admin/users/edit/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
                        return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_user_edit')), array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserController::editAction',));
                    }

                    // admin_user_delete
                    if ('/admin/users/delete' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserController::deleteAction',  '_route' => 'admin_user_delete',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/admin/login')) {
                    // _admin_login
                    if ('/admin/login' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserSessionController::loginAction',  '_route' => '_admin_login',);
                    }

                    // _admin_login_check
                    if ('/admin/login-check' === $pathinfo) {
                        return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserSessionController::loginCheckAction',  '_route' => '_admin_login_check',);
                    }

                }

                // _admin_logout
                if ('/admin/logout' === $pathinfo) {
                    return array (  '_controller' => 'V3labs\\AdminBundle\\Controller\\UserSessionController::logoutAction',  '_route' => '_admin_logout',);
                }

            }

        }

        // app_tos
        if ('/tos' === $pathinfo) {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$scheme])) {
                return $this->redirect($rawPathinfo, 'app_tos', key($requiredSchemes));
            }

            return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\DefaultController::tosAction',  '_route' => 'app_tos',);
        }

        // app_confidentiality_policy
        if ('/confidentiality-policy' === $pathinfo) {
            $requiredSchemes = array (  'http' => 0,);
            if (!isset($requiredSchemes[$scheme])) {
                return $this->redirect($rawPathinfo, 'app_confidentiality_policy', key($requiredSchemes));
            }

            return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\DefaultController::confidentialityPolicyAction',  '_route' => 'app_confidentiality_policy',);
        }

        if (0 === strpos($pathinfo, '/endorsers')) {
            // app_endorsers
            if ('/endorsers' === $pathinfo) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_endorsers', key($requiredSchemes));
                }

                return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\EndorsersController::indexAction',  '_route' => 'app_endorsers',);
            }

            // app_endorser_show
            if (preg_match('#^/endorsers/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_endorser_show', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_endorser_show')), array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\EndorsersController::showAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/events')) {
            // app_events
            if ('/events' === $pathinfo) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_events', key($requiredSchemes));
                }

                return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\EventsController::indexAction',  '_route' => 'app_events',);
            }

            // app_event_show
            if (preg_match('#^/events/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_event_show', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_event_show')), array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\EventsController::showAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/products')) {
            // app_products
            if ('/products' === $pathinfo) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_products', key($requiredSchemes));
                }

                return array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\ProductsController::indexAction',  '_route' => 'app_products',);
            }

            // app_product_show
            if (preg_match('#^/products/(?P<slug>[^/]++)$#s', $pathinfo, $matches)) {
                $requiredSchemes = array (  'http' => 0,);
                if (!isset($requiredSchemes[$scheme])) {
                    return $this->redirect($rawPathinfo, 'app_product_show', key($requiredSchemes));
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_product_show')), array (  '_locale' => 'en',  '_controller' => 'AppBundle\\Controller\\ProductsController::showAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/media/cache/resolve')) {
            // liip_imagine_filter_runtime
            if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_-]*)/rc/(?P<hash>[^/]++)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_liip_imagine_filter_runtime;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liip_imagine_filter_runtime')), array (  '_controller' => 'liip_imagine.controller:filterRuntimeAction',));
            }
            not_liip_imagine_filter_runtime:

            // liip_imagine_filter
            if (preg_match('#^/media/cache/resolve/(?P<filter>[A-z0-9_-]*)/(?P<path>.+)$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_liip_imagine_filter;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'liip_imagine_filter')), array (  '_controller' => 'liip_imagine.controller:filterAction',));
            }
            not_liip_imagine_filter:

        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
